\select@language {polish}
\contentsline {paragraph}{}{1}{section*.1}
\contentsline {paragraph}{}{1}{section*.2}
\select@language {english}
\contentsline {paragraph}{}{1}{section*.3}
\contentsline {paragraph}{}{1}{section*.4}
\select@language {polish}
\contentsline {chapter}{\numberline {1}Wst\k ep}{6}{chapter.1}
\contentsline {paragraph}{}{6}{section*.6}
\contentsline {paragraph}{}{6}{section*.7}
\contentsline {chapter}{\numberline {2}Podzia\IeC {\l } pracy}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}\IeC {\L }ukasz Dragan}{7}{section.2.1}
\contentsline {paragraph}{}{7}{section*.8}
\contentsline {section}{\numberline {2.2}Marcin Fusiara}{7}{section.2.2}
\contentsline {paragraph}{}{7}{section*.9}
\contentsline {chapter}{\numberline {3}Analiza za\IeC {\l }o\.ze\'n projektowych}{8}{chapter.3}
\contentsline {paragraph}{}{8}{section*.10}
\contentsline {section}{\numberline {3.1}U\.zytkownik}{8}{section.3.1}
\contentsline {paragraph}{}{8}{section*.11}
\contentsline {subsection}{\numberline {3.1.1}Role u\.zytkownik\'ow}{9}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Dodanie nowego u\.zytkownika}{10}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Rejestracja nowego u\.zytkownika}{11}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Wyszukiwanie u\.zytkownika}{12}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Zmiana roli istniej\k acego u\.zytkownika}{12}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Edycja danych u\.zytkownika}{12}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Dezaktywacja istniej\k acego u\.zytkownika}{13}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}Ponowna aktywacja istniej\k acego u\.zytkownika}{13}{subsection.3.1.8}
\contentsline {subsection}{\numberline {3.1.9}Logowanie do systemu}{14}{subsection.3.1.9}
\contentsline {subsection}{\numberline {3.1.10}Zmiana has\IeC {\l }a u\.zytkownika}{14}{subsection.3.1.10}
\contentsline {subsection}{\numberline {3.1.11}Reset has\IeC {\l }a u\.zytkownika}{15}{subsection.3.1.11}
\contentsline {section}{\numberline {3.2}Modu\IeC {\l } forum}{15}{section.3.2}
\contentsline {paragraph}{}{15}{section*.12}
\contentsline {paragraph}{}{15}{section*.13}
\contentsline {subsection}{\numberline {3.2.1}Uzyskanie dost\k epu do pisania na forum}{16}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Uzyskanie dost\k epu do czytania na forum}{16}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Modu\IeC {\l } stron informacyjnych}{16}{section.3.3}
\contentsline {paragraph}{}{16}{section*.14}
\contentsline {subsection}{\numberline {3.3.1}Wy\'swietlanie stron}{17}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Zmiana j\k ezyka na stronie}{18}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Dodanie nowego odno\'snika do menu nawigacyjnego}{18}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Dodanie t\IeC {\l }umaczenia dla istniej\k acej podstrony}{19}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Edycja istniej\k acej strony}{19}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Usuni\k ecie istniej\k acej strony}{19}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Dodanie nowej strony}{20}{subsection.3.3.7}
\contentsline {section}{\numberline {3.4}Modu\IeC {\l } dydaktyki}{20}{section.3.4}
\contentsline {paragraph}{}{20}{section*.15}
\contentsline {paragraph}{}{21}{section*.16}
\contentsline {subsection}{\numberline {3.4.1}Edycja strony przedmiotu}{22}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Pobranie pliku}{22}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Odczyt informacji zawartych na stronie przedmiotu}{23}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Dodanie nowego przedmiotu}{23}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Przypisanie nauczyciela do przedmiotu}{24}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Usuni\k ecie przedmiotu}{24}{subsection.3.4.6}
\contentsline {subsection}{\numberline {3.4.7}Wypisanie nauczyciela z przedmiotu}{25}{subsection.3.4.7}
\contentsline {subsection}{\numberline {3.4.8}Rejestracja na przedmiot}{25}{subsection.3.4.8}
\contentsline {section}{\numberline {3.5}Modu\IeC {\l } administratora}{26}{section.3.5}
\contentsline {paragraph}{}{26}{section*.17}
\contentsline {paragraph}{Konfiguracja menu}{26}{section*.18}
\contentsline {paragraph}{Zarz\k adzanie j\k ezykami}{26}{section*.19}
\contentsline {paragraph}{Edycja galerii}{26}{section*.20}
\contentsline {paragraph}{Konfiguracja kafelkowego menu strony g\IeC {\l }\'ownej}{26}{section*.21}
\contentsline {subsection}{\numberline {3.5.1}Edycja menu nawigacyjnego}{27}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Edycja menu kafelk\'ow}{28}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Dodanie nowego j\k ezyka}{28}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Edycja t\IeC {\l }umacze\'n tre\'sci systemu}{29}{subsection.3.5.4}
\contentsline {subsection}{\numberline {3.5.5}Edycja galerii obrazk\'ow}{29}{subsection.3.5.5}
\contentsline {section}{\numberline {3.6}Wymagania niefunkcjonalne}{29}{section.3.6}
\contentsline {chapter}{\numberline {4}Architektura systemu}{31}{chapter.4}
\contentsline {paragraph}{}{31}{section*.22}
\contentsline {paragraph}{}{31}{section*.23}
\contentsline {paragraph}{}{32}{section*.24}
\contentsline {section}{\numberline {4.1}Warstwy systemu}{32}{section.4.1}
\contentsline {paragraph}{Warstwa dost\k epu do danych}{32}{section*.25}
\contentsline {paragraph}{Warstwa logiki biznesowej}{32}{section*.26}
\contentsline {paragraph}{Warstwa interfejsu u\.zytkownika}{33}{section*.27}
\contentsline {section}{\numberline {4.2}Baza danych}{33}{section.4.2}
\contentsline {paragraph}{}{33}{section*.28}
\contentsline {paragraph}{}{34}{section*.29}
\contentsline {paragraph}{}{34}{section*.30}
\contentsline {paragraph}{}{34}{section*.31}
\contentsline {section}{\numberline {4.3}U\.zytkownicy systemu}{34}{section.4.3}
\contentsline {paragraph}{}{34}{section*.32}
\contentsline {section}{\numberline {4.4}Wieloj\k ezyczno\'s\'c}{35}{section.4.4}
\contentsline {paragraph}{}{35}{section*.33}
\contentsline {paragraph}{Zmiana wersji j\k ezykowej}{35}{section*.34}
\contentsline {paragraph}{}{36}{section*.35}
\contentsline {section}{\numberline {4.5}Projekt podstron}{36}{section.4.5}
\contentsline {paragraph}{}{36}{section*.36}
\contentsline {section}{\numberline {4.6}Integracja systemu z forum}{37}{section.4.6}
\contentsline {paragraph}{}{37}{section*.37}
\contentsline {paragraph}{Integracja uwierzytelniania}{37}{section*.38}
\contentsline {paragraph}{Integracja u\.zytkownik\'ow}{38}{section*.39}
\contentsline {section}{\numberline {4.7}Dodatkowe mechanizmy u\.zyte w systemie}{38}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Uwierzytelnianie}{38}{subsection.4.7.1}
\contentsline {paragraph}{}{38}{section*.40}
\contentsline {subsection}{\numberline {4.7.2}Walidacja AntiForgery}{38}{subsection.4.7.2}
\contentsline {paragraph}{}{38}{section*.41}
\contentsline {subsection}{\numberline {4.7.3}Cache}{39}{subsection.4.7.3}
\contentsline {paragraph}{}{39}{section*.42}
\contentsline {subsection}{\numberline {4.7.4}Stronicowanie}{39}{subsection.4.7.4}
\contentsline {paragraph}{}{39}{section*.43}
\contentsline {chapter}{\numberline {5}Proces tworzenia systemu}{40}{chapter.5}
\contentsline {section}{\numberline {5.1}Harmonogram}{40}{section.5.1}
\contentsline {paragraph}{}{40}{section*.44}
\contentsline {section}{\numberline {5.2}Metodyka wytwarzania oprogramowania}{43}{section.5.2}
\contentsline {paragraph}{}{43}{section*.45}
\contentsline {subsection}{\numberline {5.2.1}Wersjonowanie}{43}{subsection.5.2.1}
\contentsline {paragraph}{}{43}{section*.46}
\contentsline {chapter}{\numberline {6}Dokumentacja techniczna}{44}{chapter.6}
\contentsline {paragraph}{}{44}{section*.47}
\contentsline {section}{\numberline {6.1}Struktura bazy danych}{44}{section.6.1}
\contentsline {paragraph}{}{44}{section*.48}
\contentsline {section}{\numberline {6.2}Najwa\.zniejsze klasy}{47}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Model dziedziny}{47}{subsection.6.2.1}
\contentsline {paragraph}{}{47}{section*.49}
\contentsline {paragraph}{User}{48}{section*.50}
\contentsline {paragraph}{Subject}{49}{section*.51}
\contentsline {paragraph}{Language}{49}{section*.52}
\contentsline {paragraph}{PageGroup, MenuGroup}{49}{section*.53}
\contentsline {subsection}{\numberline {6.2.2}Logika biznesowa}{49}{subsection.6.2.2}
\contentsline {paragraph}{}{49}{section*.54}
\contentsline {subsection}{\numberline {6.2.3}Back-end interfejsu u\.zytkownika}{51}{subsection.6.2.3}
\contentsline {paragraph}{}{51}{section*.55}
\contentsline {paragraph}{}{53}{section*.56}
\contentsline {subsection}{\numberline {6.2.4}Front-end interfejsu u\.zytkownika}{53}{subsection.6.2.4}
\contentsline {paragraph}{}{53}{section*.57}
\contentsline {subsection}{\numberline {6.2.5}Front-end panelu administratora}{53}{subsection.6.2.5}
\contentsline {paragraph}{}{53}{section*.58}
\contentsline {paragraph}{Model}{53}{section*.59}
\contentsline {paragraph}{Kontroler - Controller}{53}{section*.60}
\contentsline {paragraph}{Widok - View}{53}{section*.61}
\contentsline {subsection}{\numberline {6.2.6}Klasy pomocnicze}{54}{subsection.6.2.6}
\contentsline {paragraph}{CacheHelper}{54}{section*.62}
\contentsline {paragraph}{ForumClient}{54}{section*.63}
\contentsline {paragraph}{SsoController}{54}{section*.64}
\contentsline {paragraph}{DomainContext}{54}{section*.65}
\contentsline {paragraph}{LanguageFilter}{54}{section*.66}
\contentsline {section}{\numberline {6.3}Zastosowane technologie i narz\k edzia}{55}{section.6.3}
\contentsline {paragraph}{C\# 5}{55}{section*.67}
\contentsline {paragraph}{Visual Studio 2012-2015}{55}{section*.68}
\contentsline {paragraph}{ASP.NET MVC 5}{55}{section*.69}
\contentsline {paragraph}{Webapi 2}{55}{section*.70}
\contentsline {paragraph}{MS SQL Server 2012}{55}{section*.71}
\contentsline {paragraph}{HTML5, CSS3, JavaScript}{55}{section*.72}
\contentsline {paragraph}{Tiny MCE 4.2.7}{55}{section*.73}
\contentsline {paragraph}{jQuery}{56}{section*.74}
\contentsline {paragraph}{Zurb Foundation}{56}{section*.75}
\contentsline {paragraph}{AngularJS}{56}{section*.76}
\contentsline {paragraph}{Autofac 3.4.0}{56}{section*.77}
\contentsline {paragraph}{EntityFramework 6.1.3}{56}{section*.78}
\contentsline {paragraph}{Moq}{56}{section*.79}
\contentsline {paragraph}{NUnit 2.6}{57}{section*.80}
\contentsline {paragraph}{MVCForum}{57}{section*.81}
\contentsline {chapter}{\numberline {7}Podsumowanie}{58}{chapter.7}
\contentsline {paragraph}{}{58}{section*.82}
\contentsline {paragraph}{}{58}{section*.83}
\contentsline {paragraph}{}{58}{section*.84}
\contentsline {paragraph}{}{59}{section*.85}
\contentsline {paragraph}{}{59}{section*.86}
\contentsline {paragraph}{}{59}{section*.87}
\contentsline {paragraph}{}{59}{section*.88}
\contentsline {chapter}{\numberline {8}S\IeC {\l }ownik poj\k e\'c}{60}{chapter.8}
\contentsline {paragraph}{}{60}{section*.89}
\contentsline {chapter}{\numberline {A}Instrukcja u\.zytkownika}{67}{appendix.A}
\contentsline {paragraph}{}{67}{section*.91}
\contentsline {section}{\numberline {A.1}Dost\k ep do aplikacji}{67}{section.A.1}
\contentsline {paragraph}{}{67}{section*.92}
\contentsline {section}{\numberline {A.2}Podstawowe elementy systemu}{68}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Strona g\IeC {\l }\'owna}{68}{subsection.A.2.1}
\contentsline {paragraph}{}{68}{section*.93}
\contentsline {paragraph}{Logo uczelni (1)}{68}{section*.94}
\contentsline {paragraph}{Menu g\IeC {\l }\'owne (2)}{68}{section*.95}
\contentsline {paragraph}{Baner (3)}{68}{section*.96}
\contentsline {paragraph}{Menu 2. (4)}{68}{section*.97}
\contentsline {paragraph}{Menu kafelkowe (5)}{68}{section*.98}
\contentsline {paragraph}{Stopka (6)}{69}{section*.99}
\contentsline {subsection}{\numberline {A.2.2}Ekran logowania}{69}{subsection.A.2.2}
\contentsline {paragraph}{}{69}{section*.100}
\contentsline {subsection}{\numberline {A.2.3}Zmiana j\k ezyka systemu}{70}{subsection.A.2.3}
\contentsline {paragraph}{}{70}{section*.101}
\contentsline {section}{\numberline {A.3}Strony informacyjne}{70}{section.A.3}
\contentsline {section}{\numberline {A.4}Dydaktyka}{70}{section.A.4}
\contentsline {paragraph}{}{70}{section*.102}
\contentsline {paragraph}{}{71}{section*.103}
\contentsline {section}{\numberline {A.5}Forum}{73}{section.A.5}
\contentsline {paragraph}{Administrator}{73}{section*.104}
\contentsline {paragraph}{Standardowy u\.zytkownik}{73}{section*.105}
\contentsline {paragraph}{Go\'s\'c}{73}{section*.106}
\contentsline {section}{\numberline {A.6}Panel administratora}{73}{section.A.6}
\contentsline {subsection}{\numberline {A.6.1}J\k ezyki}{74}{subsection.A.6.1}
\contentsline {paragraph}{Komunikaty systemu}{74}{section*.107}
\contentsline {paragraph}{Strony informacyjne}{74}{section*.108}
\contentsline {subsection}{\numberline {A.6.2}U\.zytkownicy}{75}{subsection.A.6.2}
\contentsline {paragraph}{Nowy u\.zytkownik}{75}{section*.109}
\contentsline {paragraph}{Istniej\k acy u\.zytkownik}{75}{section*.110}
\contentsline {subsection}{\numberline {A.6.3}Strony}{76}{subsection.A.6.3}
\contentsline {paragraph}{Nowa strona}{76}{section*.111}
\contentsline {paragraph}{Istniej\k aca strona}{77}{section*.112}
\contentsline {subsection}{\numberline {A.6.4}Strona g\IeC {\l }\'owna}{77}{subsection.A.6.4}
\contentsline {paragraph}{}{77}{section*.113}
\contentsline {paragraph}{Edycja listy kafelk\'ow}{78}{section*.114}
\contentsline {paragraph}{Edycja kafelka}{78}{section*.115}
\contentsline {subsection}{\numberline {A.6.5}Menu nawigacyjne}{79}{subsection.A.6.5}
\contentsline {subsection}{\numberline {A.6.6}Przedmioty}{79}{subsection.A.6.6}
\contentsline {paragraph}{Nowy przedmiot}{79}{section*.116}
\contentsline {paragraph}{Edycja przedmiotu}{80}{section*.117}
\contentsline {paragraph}{Edycja sekcji nale\.z\k acych do przedmiotu}{80}{section*.118}
\contentsline {paragraph}{Edycja aktualno\'sci}{81}{section*.119}
\contentsline {paragraph}{Edycja planu zaj\k e\'c i sylabusa}{81}{section*.120}
\contentsline {paragraph}{Dodawanie materia\IeC {\l }\'ow dydaktycznych}{82}{section*.121}
\contentsline {paragraph}{Przypisywanie nauczyciela do przedmiotu}{82}{section*.122}
\contentsline {paragraph}{Wy\'swietlanie listy student\'ow zapisanych na przedmiot}{82}{section*.123}
\contentsline {paragraph}{Zarz\k adzanie statusem zapisania studenta na przedmiot}{83}{section*.124}
\contentsline {subsection}{\numberline {A.6.7}Galeria obrazk\'ow}{83}{subsection.A.6.7}
\contentsline {paragraph}{U\.zycie obrazka z galerii}{83}{section*.125}
\contentsline {chapter}{\numberline {B}Testy systemu}{85}{appendix.B}
\contentsline {paragraph}{}{85}{section*.126}
\contentsline {paragraph}{}{85}{section*.127}
\contentsline {section}{\numberline {B.1}Testy konta u\.zytkownika}{85}{section.B.1}
\contentsline {paragraph}{U\.zytkownik mo\.ze si\k e zalogowa\'c}{85}{section*.128}
\contentsline {paragraph}{U\.zytkownik mo\.ze zresetowa\'c has\IeC {\l }o}{86}{section*.129}
\contentsline {paragraph}{U\.zytkownik mo\.ze zmieni\'c has\IeC {\l }o}{86}{section*.130}
\contentsline {paragraph}{Administrator mo\.ze wyszuka\'c istniej\k acego u\.zytkownika}{87}{section*.131}
\contentsline {paragraph}{Administrator mo\.ze doda\'c nowego u\.zytkownika}{87}{section*.132}
\contentsline {paragraph}{Administrator mo\.ze zmieni\'c rol\k e istniej\k acego u\.zytkownika}{88}{section*.133}
\contentsline {paragraph}{Administrator mo\.ze dezaktywowa\'c konto u\.zytkownika}{88}{section*.134}
\contentsline {section}{\numberline {B.2}Testy forum}{89}{section.B.2}
\contentsline {paragraph}{U\.zytkownik mo\.ze uzyska\'c dost\k ep do forum}{89}{section*.135}
\contentsline {section}{\numberline {B.3}Testy stron informacyjnych}{89}{section.B.3}
\contentsline {paragraph}{Administrator mo\.ze doda\'c now\k a stron\k e}{89}{section*.136}
\contentsline {paragraph}{U\.zytkownik mo\.ze wy\'swietla\'c strony informacyjne}{90}{section*.137}
\contentsline {paragraph}{U\.zytkownik mo\.ze zmieni\'c j\k ezyk strony}{90}{section*.138}
\contentsline {paragraph}{Administrator mo\.ze doda\'c t\IeC {\l }umaczenie strony}{91}{section*.139}
\contentsline {paragraph}{Administrator mo\.ze edytowa\'c zawarto\'s\'c istniej\k acej strony}{91}{section*.140}
\contentsline {paragraph}{Administrator mo\.ze usun\k a\'c istniej\k ac\k a stron\k e}{92}{section*.141}
\contentsline {section}{\numberline {B.4}Testy modu\IeC {\l }u dydaktyki}{92}{section.B.4}
\contentsline {paragraph}{Nauczyciel mo\.ze edytowa\'c stron\k e przedmiotu}{92}{section*.142}
\contentsline {paragraph}{Ucze\'n posiadaj\k acy dost\k ep do przedmiotu mo\.ze odczyta\'c zawarto\'s\'c przedmiotu}{93}{section*.143}
\contentsline {paragraph}{Ucze\'n posiadaj\k acy dost\k ep do przedmiotu mo\.ze pobra\'c plik}{93}{section*.144}
\contentsline {paragraph}{Ucze\'n nieposiadaj\k acy dost\k epu do przedmiotu nie mo\.ze pobra\'c pliku}{94}{section*.145}
\contentsline {paragraph}{Administrator mo\.ze doda\'c nowy przedmiot}{94}{section*.146}
\contentsline {paragraph}{Administrator mo\.ze przydzieli\'c nauczyciela do przedmiotu}{94}{section*.147}
\contentsline {paragraph}{Zapisanie si\k e na przedmiot}{95}{section*.148}
\contentsline {paragraph}{Administrator mo\.ze odebra\'c nauczycielowi prawa do przedmiotu}{96}{section*.149}
\contentsline {section}{\numberline {B.5}Testy modu\IeC {\l }u administratora}{97}{section.B.5}
\contentsline {paragraph}{Administrator mo\.ze edytowa\'c menu nawigacyjne}{97}{section*.150}
\contentsline {paragraph}{Administrator mo\.ze doda\'c nowy j\k ezyk do systemu}{97}{section*.151}
