﻿using System;

namespace UniversityWebsite.Services.Exceptions
{
    /// <summary>
    /// Wyjątek wywoływany przy niepowodzeniu walidacji danych na poziomie logiki biznesowej.
    /// </summary>
    public class PropertyValidationException : Exception
    {
        /// <summary>
        /// Nazwa właściwości, która nie przeszła walidacji.
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// Wiadomość walidacji.
        /// </summary>
        public string PropertyValidationMessage { get; set; }

        /// <summary>
        /// Tworzy nową instancję wyjątku.
        /// </summary>
        /// <param name="propertyName">Nazwa właściwości</param>
        /// <param name="propertyValidationMessage">Wiadomość walidacji</param>
        public PropertyValidationException(string propertyName, string propertyValidationMessage)
        {
            PropertyName = propertyName;
            PropertyValidationMessage = propertyValidationMessage;
        }
    }
}
