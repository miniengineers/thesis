﻿using System;

namespace UniversityWebsite.Services.Exceptions
{
    /// <summary>
    /// Wyjątek zgłaszany, gdy encja potrzebna do wykonania operacji logiki biznesowej nie znajduje się w bazie danych.
    /// </summary>
    public class NotFoundException : Exception
    {
        /// <summary>
        /// Tworzy nową instancję wyjątku.
        /// </summary>
        /// <param name="message">Treść wiadomości.</param>
        public NotFoundException(string message)
            : base(message)
        {
                
        }
    }
}