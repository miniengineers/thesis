﻿using System;
using Microsoft.AspNet.Identity;

namespace UniversityWebsite.Services.Exceptions
{
    /// <summary>
    /// Wyjątek wywoływany, gdy nie powiedzie się operacja związana z biblioteką Microsoft.AspNet.Identity
    /// </summary>
    public class IdentityOperationFailedException : Exception
    {
        /// <summary>
        /// Tworzy nową instancję wyjątku.
        /// </summary>
        /// <param name="identityResult">Obiekt zwracany przez metody biblioteki Microsoft.AspNet.Identity</param>
        public IdentityOperationFailedException(IdentityResult identityResult)
        {
            IdentityResult = identityResult;
        }
        /// <summary>
        /// Obiekt zwracany przez metody biblioteki Microsoft.AspNet.Identity
        /// </summary>
        public IdentityResult IdentityResult { get; private set; }
    }
}