﻿using System;
using System.Web.Security;

namespace UniversityWebsite.Services.Helpers
{
    /// <summary>
    /// Wspiera proces tworzenia nowego konta użytkownika.
    /// </summary>
    public static class PasswordGenerator
    {
        /// <summary>
        /// Generuje losowe hasło o podanej długości zawierające przynajmniej jedną dużą literę, jedną małą i jedną cyfrę i dwa znaki niealfanumeryczne.
        /// </summary>
        /// <param name="length">Żądana długość hasła</param>
        /// <returns>Wartość hasła.</returns>
        /// <exception cref="ArgumentException">Wyjątek zwracany, gdy żądana długość hasła jest krótsza niż 5</exception>
        public static string GeneratePassword(int length)
        {
            if(length<5) throw new ArgumentException("length cannot be shorter than 5");
            Random r = new Random();
            string password = Membership.GeneratePassword(length - 3, 2) + (char)('A' + r.Next(26)) + (char)('a' + r.Next(26)) + (char)('0' + r.Next(10));
            return password;
        }
    }
}
