﻿using System;
using System.Runtime.Caching;

namespace UniversityWebsite.Services.Helpers
{
    /// <summary>
    /// Wspomaga przechowywanie danych w pamięci cache.
    /// </summary>
    public class CacheHelper
    {
        private static readonly object O = new object();
        /// <summary>
        /// Dodaje obiekt do pamięcia cache.
        /// </summary>
        /// <param name="key">Unikalny lucz</param>
        /// <param name="value">Obiekt do przechowania</param>
        /// <param name="lifeTime">Czas, jak długo obiekt ma pozostać w pamięci.</param>
        public static void Add(string key, object value, TimeSpan lifeTime)
        {
            lock (O)
                MemoryCache.Default.Add(key, value??Null, new CacheItemPolicy { AbsoluteExpiration = DateTime.Now + lifeTime });
        }

        /// <summary>
        /// Pobiera obiekt przechowywany w pamięci cache.
        /// </summary>
        /// <param name="key">Unikalny klucz obiektu.</param>
        /// <typeparam name="T">Typ przechowywanego obiektu.</typeparam>
        /// <returns>Obiekt przechowywany w pamięci.</returns>
        public static T Get<T>(string key)
            where T : class
        {
            lock (O)
            {
                var result = MemoryCache.Default.Get(key);
                if (result == Null)
                    result = null;
                return (T)result;
            }
        }

        /// <summary>
        /// Pobiera obiekt z pamięci cache lub jeżeli w pamięci nie ma obiektu o danym kluczu wywołuje daną metodę i zapisuje w pamięci zwrócony przez nią obiekt.
        /// </summary>
        /// <param name="key">Unikalny klucz obiektu</param>
        /// <param name="function">Metoda do wywołania</param>
        /// <param name="lifeTime">Czas, jak długo obiekt zwracany przez metodę ma pozostać w pamięci.</param>
        /// <typeparam name="T">Typ Zwracanego obiektu</typeparam>
        /// <returns>Obiekt zwracany przez daną metodę lub obiekt z pamięci cache.</returns>
        public static T GetOrInvoke<T>(string key, Func<object> function, TimeSpan lifeTime)
            where T : class
        {
            lock (O)
            {
                if (MemoryCache.Default.Contains(key))
                    return Get<T>(key);
                object value = function();
                Add(key, value, lifeTime);
                return (T)value;
            }
        }

        /// <summary>
        /// Usuwa obiekt o danym kluczu z pamięci cache.
        /// </summary>
        /// <param name="key">Unikalny klucz obiektu.</param>
        public static void Remove(string key)
        {
            lock (O)
                MemoryCache.Default.Remove(key);
        }

        private static readonly object Null = new object();

        //private static TimeSpan GetTimeSpan(LifeTime lifeTime)
        //{
        //    switch (lifeTime)
        //    {
        //        case LifeTime.Hour:
        //            return TimeSpan.FromHours(1);
        //        case LifeTime.Day:
        //            return TimeSpan.FromDays(1);
        //        case LifeTime.Week:
        //            return TimeSpan.FromDays(7);
        //        case LifeTime.Month:
        //            return TimeSpan.FromDays(30);
        //        case LifeTime.Year:
        //            return TimeSpan.FromDays(365);
        //    }
        //}

        //public enum LifeTime
        //{
        //    Hour = 0,
        //    Day = 1,
        //    Week = 2,
        //    Month = 3,
        //    Year = 4
        //}
    }

    /// <summary>
    /// Zawiera wykorzystywane w projekcie klucze obiektów w pamięci cache.
    /// </summary>
    public static class CacheKeys
    {
        public static string MenuKey = "MenuKey_{0}_{1}";
        public static string LanguageAllKey = "LanguageAll";
        public static string DictionaryPhraseKey = "DictionaryPhrase_{0}_{1}";
        public static string DictionaryAllKeysKey = "DictionaryAllKeys";
        public static string PagesMenuKey = "PagesMenu_{0}";
        public static string TeachingMenuKey = "TeachingMenu";
    }
}
