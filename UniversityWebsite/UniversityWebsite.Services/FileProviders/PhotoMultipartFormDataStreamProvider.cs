﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;

namespace UniversityWebsite.Services.FileProviders
{
    /// <summary>
    /// Pomaga przy zapisie pliku graficznego dostarczonego za pomocą protokołu http.
    /// </summary>
    public class PhotoMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        private readonly string _fileId;

        /// <summary>
        /// Nazwa pliku.
        /// </summary>
        public string Name { get; private set; }

        private readonly string[] _imageExts= {".bmp", ".jpg", ".png", ".gif", ".tiff", ".jpeg"};

        /// <summary>
        /// Tworzy nową instancję klasy.
        /// </summary>
        /// <param name="path">Ścieżka do miejsca zapisania pliku graficznego.</param>
        /// <param name="fileId">Id pliku.</param>
        public PhotoMultipartFormDataStreamProvider(string path, string fileId)
            : base(path)
        {
            _fileId = fileId;
        }

        public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
        {
            Name = headers.ContentDisposition.FileName.Trim(new []{'"'});
            if (Name == null)
                throw new ArgumentException("Name cannot be null");
            var ext = Path.GetExtension(Name);
            if(!_imageExts.Contains(ext.ToLower()))
                throw new ArgumentException("Wrong content type.");
            return _fileId;
        }
    }
}