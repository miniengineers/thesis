﻿using System;
using System.Net.Http;

namespace UniversityWebsite.Services.FileProviders
{
    /// <summary>
    /// Pomaga przy zapisie pliku dostarczonego za pomocą protokołu http.
    /// </summary>
    public class FileMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        private readonly string _fileId;

        /// <summary>
        /// Nazwa pliku.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Tworzy nową instancję klasy.
        /// </summary>
        /// <param name="path">Ścieżka do miejsca zapisania pliku.</param>
        /// <param name="fileId">Id pliku.</param>
        public FileMultipartFormDataStreamProvider(string path, string fileId)
            : base(path)
        {
            _fileId = fileId;
        }

        public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
        {
            Name = headers.ContentDisposition.FileName.Trim(new []{'"'});
            if(Name==null)
                throw new ArgumentException("Name cannot be null");
            return _fileId;
        }
    }
}