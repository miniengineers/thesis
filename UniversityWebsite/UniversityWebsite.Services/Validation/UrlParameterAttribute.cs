﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace UniversityWebsite.Services.Validation
{
    /// <summary>
    /// Atrybut wykorzystywany do walidacji poprawności tekstu będącego wartością atrybutu przekazywanego przez url.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class UrlParameterAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "Wyrażenie zawiera niepoprawne znaki.";

        /// <summary>
        /// Tworzy nową instancję atrybutu.
        /// </summary>
        public UrlParameterAttribute()
            : base(DefaultErrorMessage)
        {
        }

        /// <summary>
        /// Formatuje wiadomość walidacji.
        /// </summary>
        /// <param name="name">Nazwa błędu walidacji.</param>
        /// <returns>Sformatowana wiadomosć walidacji.</returns>
        public override string FormatErrorMessage(string name)
        {
            return ErrorMessageString;
        }

        /// <summary>
        /// Sprawdza, czy właściwość spełnia walidację.
        /// </summary>
        /// <param name="value">Wartość właściwości</param>
        /// <param name="validationContext">Kontekst walidacji</param>
        /// <returns>Informacja, czy właściwość spełnia walidację.</returns>
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            if (value == null) return ValidationResult.Success;
            Regex regex = new Regex(@"^[a-zA-Z][a-zA-Z0-9\-][a-zA-Z0-9]*$");
            Match match = regex.Match(value.ToString());
            if (match.Success)
                return ValidationResult.Success;
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }

}