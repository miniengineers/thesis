﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper.QueryableExtensions;
using UniversityWebsite.Core;
using UniversityWebsite.Domain.Model;
using UniversityWebsite.Services.Exceptions;
using UniversityWebsite.Services.Helpers;
using UniversityWebsite.Services.Model;

namespace UniversityWebsite.Services
{
    /// <summary>
    /// Serwis realizujący logikę biznesową dotyczącą stron systemu.
    /// </summary>
    public interface IPageService
    {
        /// <summary>
        /// Wyszukuje stronę o podanym UrlName.
        /// </summary>
        /// <param name="name">UrlName strony</param>
        /// <returns>Strona o podanym UrlName lub null</returns>
        PageDto FindPage(string name);
        /// <summary>
        /// Wyszukuje stronę o podanym Id.
        /// </summary>
        /// <param name="id">Id szukanej strony.</param>
        /// <returns>Strona o podanym Id.</returns>
        PageDto FindPage(int id);
        /// <summary>
        /// Wyszukuje tłumaczenie strony o podanym UrlName w danym języku
        /// </summary>
        /// <param name="name">UrlName strony</param>
        /// <param name="countryCode">język tłumaczenia strony</param>
        /// <returns>tłumaczenie danej strony w danym języku lub null, gdy tłumaczenie nie istnieje.</returns>
        PageDto FindTranslation(string name, string countryCode);
        /// <summary>
        /// Wyszukuje tłumaczenie strony o podanym Id w danym języku
        /// </summary>
        /// <param name="id">Id strony</param>
        /// <param name="countryCode">język tłumaczenia strony</param>
        /// <returns>tłumaczenie danej strony w danym języku lub null, gdy tłumaczenie nie istnieje.</returns>
        PageDto FindTranslation(int id, string countryCode);
        /// <summary>
        /// Wyszukuje wszystkie tłumaczenia strony o danym UrlName.
        /// </summary>
        /// <param name="name">UrlName strony</param>
        /// <returns>Wyliczenie wszystkich tłumaczeń danej strony</returns>
        IEnumerable<PageDto> GetTranslations(string name);
        /// <summary>
        /// Zwraca wszystkie strony serwisu.
        /// </summary>
        /// <param name="limit">Maksymalna liczba zwróconych stron</param>
        /// <param name="offset">Numer porządkowy pierwszej strony, która ma zostać zwrócona</param>
        /// <param name="countryCode">Język, w którym mają zostać wyszukane strony</param>
        /// <param name="pattern">Podciąg, który mają zawierać strony w tytule</param>
        /// <returns>Zbiór stron</returns>
        IEnumerable<PageDto> GetAll(string countryCode, string pattern, int limit, int offset);
        /// <summary>
        /// Zwraca liczbę wszystkich stron serwisu o danym języku i zawierających dany podciąg w tytule.
        /// </summary>
        /// <param name="countryCode">Język, w którym mają zostać wyszukane strony</param>
        /// <param name="pattern">Podciąg, który mają zawierać strony w tytule</param>
        /// <returns>Liczba natoralna</returns>
        int GetPagesNumber(string countryCode, string pattern);
        /// <summary>
        /// Aktualizuje zawartość strony.
        /// </summary>
        /// <param name="page">Strona wraz z zawartością, która ma nadpisać obecną.</param>
        void UpdateContent(PageDto page);
        /// <summary>
        /// Dodaje nową stronę do systemu
        /// </summary>
        /// <param name="page">Dodawana strona.</param>
        /// <returns>Dodana strona z bazy danych.</returns>
        PageDto Add(PageDto page);
        /// <summary>
        /// Aktualizuje istniejącą stronę nadpisując jej pola zawartymi w <paramref name="page"/> niebędącymi
        /// </summary>
        /// <param name="page">dane strony, którymi zostaną nadpisane aktualne.</param>
        /// <returns>Zaktualizowana strona z bazy danych.</returns>
        PageDto UpdatePage(PageDto page);
        /// <summary>
        /// Usuwa stronę z serwisu.
        /// </summary>
        /// <param name="id">Id strony do usunięcia.</param>
        void Delete(int id);
        /// <summary>
        /// Usuwa grupę stron z serwisu.
        /// </summary>
        /// <param name="pageId">Id grupy stron do usunięcia.</param>
        void DeleteGroup(int pageId);
        /// <summary>
        /// Wyszukuje języki na które istnieją w systemie tłumaczenia danej strony.
        /// </summary>
        /// <param name="id">Id danej strony</param>
        /// <returns>Wyliczenie kodów językowych</returns>
        IEnumerable<string> GetTranslationsLanguages(int id);
        /// <summary>
        /// Zwraca zbiór stron o danym języku.
        /// </summary>
        /// <param name="countryCode">Id języka zwracanych stron</param>
        /// <param name="limit">Maksymalna liczba zwróconych stron</param>
        /// <param name="offset">Numer porządkowy pierwszej strony, która ma zostać zwrócona</param>
        /// <returns>Zbiór stron</returns>
        IEnumerable<PageDto> GetPagesByCountryCode(string countryCode, int limit, int offset);
        /// <summary>
        /// Zwraca liczbę stron o danym języku.
        /// </summary>
        /// <param name="countryCode">Id języka zwracanych stron</param>
        /// <returns>Liczba naturalna.</returns>
        int GetPagesNumberByCountryCode(string countryCode);
    }
    /// <summary>
    /// Implementacja serwisu realizującego logikę biznesową dotyczącą stron systemu.
    /// </summary>
    public class PageService : IPageService
    {
        private IDomainContext _context;
        private const int SameTitlePagesMaxNumber = 100;
        /// <summary>
        /// Tworzy nową instancję serwisu.
        /// </summary>
        /// <param name="context"></param>
        public PageService(IDomainContext context = null)
        {
            _context = context ?? new DomainContext();
        }

        public PageDto FindPage(int id)
        {
            var page = _context.Pages.Include("Group.Pages").SingleOrDefault(p => p.Id == id);
            if (page == null)
                return null;
            return new PageDto
            {
                Content = page.Content,
                Title = page.Title,
                Id = page.Id,
                UrlName = page.UrlName,
                CountryCode = page.CountryCode,
                CreationDate = page.CreationDate,
                Description = page.Description,
                GroupId = page.GroupId,
                LastUpdateDate = page.LastUpdateDate,
                Parent =
                    page.Parent == null
                        ? null
                        : new ParentDto
                        {
                            Id = page.Parent.Id,
                            Title = page.Parent.Title,
                            UrlName = page.Parent.UrlName,
                        },
                Translations = page.Group.Pages
                    .Where(translation => translation.Id != page.Id)
                    .Select(translation =>
                        new PageDto
                        {
                            Id = translation.Id,
                            Title = translation.Title,
                            UrlName = translation.UrlName,
                            CountryCode = translation.CountryCode
                        })
            };
        }

        public PageDto FindPage(string name)
        {
            var pages = _context.Pages.Where(p => String.Compare(p.UrlName, name, StringComparison.OrdinalIgnoreCase) == 0)
                .ProjectTo<PageDto>();
            return pages.SingleOrDefault();
        }

        public PageDto FindTranslation(int id, string countryCode)
        {
            var page = _context.Pages.SingleOrDefault(p => p.Id == id);
            if (page == null)
                throw new NotFoundException("Page with id: " + id);
            var foundPages = _context.Pages.Where(p => p.GroupId == page.GroupId && p.CountryCode == countryCode).ProjectTo<PageDto>();
            if (!foundPages.Any())
                throw new NotFoundException("Page with groupid: " + page.GroupId + " and id: " + id);
            return foundPages.SingleOrDefault();
        }
        public PageDto FindTranslation(string name, string countryCode)
        {
            var page = _context.Pages.SingleOrDefault(p => String.Compare(p.UrlName, name, StringComparison.OrdinalIgnoreCase) == 0);
            if (page == null)
                throw new NotFoundException("Page with name: " + name);
            var foundPages = _context.Pages.Where(p => p.GroupId == page.GroupId && p.CountryCode == countryCode).ProjectTo<PageDto>();
            if (!foundPages.Any())
                throw new NotFoundException("Page with groupid: " + page.GroupId + " and name: " + name);
            return foundPages.SingleOrDefault();
        }

        public IEnumerable<PageDto> GetTranslations(string name)
        {
            var page = _context.Pages.SingleOrDefault(p => p.UrlName == name);
            if (page == null)
                throw new NotFoundException("Page with name: " + name);
            return _context.Pages.Where(p => p.GroupId == page.GroupId).ProjectTo<PageDto>();
        }
        public IEnumerable<PageDto> GetAll(string countryCode, string pattern, int limit, int offset)
        {
            return _context
                .Pages
                .Where(p => (countryCode == null || p.CountryCode == countryCode) && (pattern == null || p.Title.Contains(pattern))).OrderBy(p => p.Title)
                .Skip(offset)
                .Take(limit)
                .ProjectTo<PageDto>();
        }

        public int GetPagesNumber(string countryCode, string pattern)
        {
            return _context
                .Pages
                .Count(p => (countryCode == null || p.CountryCode == countryCode) && (pattern == null || p.Title.Contains(pattern)));
        }

        public void UpdateContent(PageDto page)
        {
            var dbPage = _context
                .Pages
                .SingleOrDefault(p => p.UrlName == page.UrlName);
            if (dbPage == null)
                throw new NotFoundException("Page with urlname: " + page.UrlName);
            dbPage.Content = page.Content;
            dbPage.LastUpdateDate = DateTime.Now;
            _context.SetModified(dbPage);
            _context.SaveChanges();
        }

        private void ValidateLanguageUniqueness(string countryCode, int groupId)
        {
            if (_context.Pages.Any(p => p.CountryCode == countryCode && p.GroupId == groupId))
                throw new PropertyValidationException("page.CountryCode", string.Format("Page with countryCode: {0} and groupId: {1} already exists.", countryCode, groupId));
        }

        public PageDto Add(PageDto page)
        {
            Page newParent = null;
            if (page.Parent != null)
            {
                newParent = _context.Pages.SingleOrDefault(p => p.Id == page.Parent.Id);
                if (newParent == null)
                    throw new NotFoundException("Parent with code: " + page.Parent.UrlName);
            }
            PageGroup group = null;
            if (!page.GroupId.HasValue)
                group = _context.PageGroups.Add(new PageGroup());
            else
            {
                ValidateLanguageUniqueness(page.CountryCode, page.GroupId.Value);
                group = _context.PageGroups.SingleOrDefault(g => g.Id == page.GroupId.Value);
                if (group == null)
                    throw new NotFoundException("PageGroup with id: " + page.GroupId.Value);
            }

            var language = _context.Languages.SingleOrDefault(l => l.CountryCode == page.CountryCode);
            if (language == null)
                throw new NotFoundException("Language with countryCode: " + page.CountryCode);
            Page newPage = new Page
            {
                Language = language,
                Parent = newParent,
                Content = page.Content,
                LastUpdateDate = DateTime.Now,
                CreationDate = DateTime.Now,
                Title = page.Title,
                UrlName = page.UrlName ?? PrepareUniqueUrlName(page.Title),
                Group = group,
                Description = page.Description
            };
            Page createdPage = _context.Pages.Add(newPage);
            _context.SaveChanges();

            CacheHelper.Remove(string.Format(CacheKeys.PagesMenuKey, createdPage.CountryCode));
            return FindPage(createdPage.Id);
        }

        private string PrepareUniqueUrlName(string baseUrlName)
        {
            if (!_context.Pages.Any(p => p.UrlName == baseUrlName))
                return baseUrlName;
            for (int i = 2; i < SameTitlePagesMaxNumber; i++)
            {
                string bufName = baseUrlName + i;
                if (!_context.Pages.Any(p => p.UrlName == bufName))
                    return bufName;
            }
            throw new PropertyValidationException("page.UrlName", "Number of pages with same title exceeded");
        }

        public PageDto UpdatePage(PageDto page)
        {
            var dbPage = _context
                .Pages
                .SingleOrDefault(p => p.Id == page.Id);
            if (dbPage == null)
                throw new NotFoundException("Page o id: " + page.Id);
            if (page.CountryCode != null)
            {
                if (dbPage.CountryCode != page.CountryCode || page.GroupId != dbPage.GroupId)
                    ValidateLanguageUniqueness(page.CountryCode, dbPage.GroupId);
                dbPage.CountryCode = page.CountryCode;

                dbPage.Language = _context.Languages.FirstOrDefault(l => l.CountryCode == dbPage.CountryCode);
                dbPage.Group = _context.PageGroups.FirstOrDefault(g => g.Id == dbPage.GroupId);
            }
            if (page.Parent != null)
            {
                var newParent = _context.Pages.SingleOrDefault(p => p.Id == page.Parent.Id);
                if (newParent == null)
                    throw new NotFoundException("Parent o kodzie: " + page.Parent.UrlName);
                if (newParent.CountryCode != dbPage.CountryCode)
                    throw new PropertyValidationException("page.parent", "Parent must have the same language");
                dbPage.Parent = newParent;
            }
            if (page.Content != null)
                dbPage.Content = page.Content;
            dbPage.LastUpdateDate = DateTime.Now;
            if (page.Title != null)
                dbPage.Title = page.Title;
            if (page.Description != null)
                dbPage.Description = page.Title;
            if (page.UrlName != null)
            {
                if (!_context.Pages.Any(p => p.UrlName == page.UrlName && p.Id != page.Id))
                    dbPage.UrlName = page.UrlName;
                else throw new PropertyValidationException("page.UrlName", "Page with given urlname already exists");
            }
            else page.UrlName = PrepareUniqueUrlName(page.Title);
            _context.SetModified(dbPage);

            _context.SaveChanges();
            CacheHelper.Remove(string.Format(CacheKeys.PagesMenuKey, dbPage.CountryCode));
            return FindPage(dbPage.Id);
        }

        public void Delete(int id)
        {
            Page page = _context.Pages.FirstOrDefault(p => p.Id == id);
            if (page == null)
                throw new NotFoundException("Page with id: " + id);
            CacheHelper.Remove(string.Format(CacheKeys.PagesMenuKey, page.CountryCode));

            if (_context.Pages.Any(p => p.ParentId == id))
                throw new Exception("The page has children. Delete children first.");

            _context.SetDeleted(page);
            _context.SaveChanges();
        }

        public void DeleteGroup(int pageId)
        {
            _context.InTransaction(() =>
            {
                Page page = _context.Pages.Include(p1 => p1.Group).SingleOrDefault(p2 => p2.Id == pageId);
                if (page == null)
                    throw new NotFoundException("Page with id: " + pageId);
                CacheHelper.Remove(string.Format(CacheKeys.PagesMenuKey, page.CountryCode));

                var pagesToDelete = _context.Pages.Where(p => p.GroupId == page.GroupId);
                foreach (var dbPage in pagesToDelete)
                {
                    if (_context.Pages.Any(p => p.ParentId == dbPage.Id))
                        throw new Exception("The page with id: " + dbPage.Id + " has children. Delete children first.");
                    _context.SetDeleted(dbPage);
                }
                _context.SetDeleted(page.Group);
                _context.SaveChanges();
            });
        }

        public IEnumerable<string> GetTranslationsLanguages(int id)
        {
            Page page = _context.Pages.FirstOrDefault(p => p.Id == id);
            if (page == null)
                throw new NotFoundException("Page with id: " + id);
            return _context.Pages.Where(p => p.GroupId == page.GroupId).Select(p => p.CountryCode);
        }

        public IEnumerable<PageDto> GetPagesByCountryCode(string countryCode, int limit, int offset)
        {
            return _context.Pages.Where(p => p.CountryCode == countryCode)
                .OrderBy(p => p.Title)
                .Skip(offset)
                .Take(limit)
                .ProjectTo<PageDto>(); ;
        }

        public int GetPagesNumberByCountryCode(string countryCode)
        {
            return _context.Pages.Count(p => p.CountryCode == countryCode);
        }
    }
}
