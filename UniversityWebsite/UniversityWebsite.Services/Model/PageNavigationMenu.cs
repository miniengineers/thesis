﻿using System.Collections.Generic;

namespace UniversityWebsite.Services.Model
{
    public class PageNavigationMenu
    {
        public PageNavigationMenu()
        {
            Items = new List<PageMenuItem>();
        }
        public List<PageMenuItem> Items { get; set; }
    }
}
