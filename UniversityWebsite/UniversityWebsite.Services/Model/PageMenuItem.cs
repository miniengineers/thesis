﻿using System.Collections.Generic;

namespace UniversityWebsite.Services.Model
{
    public class PageMenuItem
    {
        public PageMenuItem(string title, string urlName)
        {
            Title = title;
            UrlName = urlName;
            Items = new List<PageMenuItem>();
        }

        public string Title { get; set; }
        public string UrlName { get; set; }
        public List<PageMenuItem> Items { get; set; }
    }
}
