﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityWebsite.Services.Model
{
    public class TeachingMenuItem
    {
        public string Title { get; set; }
        public string UrlName { get; set; }
    }
}
