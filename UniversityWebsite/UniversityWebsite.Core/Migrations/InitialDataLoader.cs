﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using UniversityWebsite.Domain.Model;

namespace UniversityWebsite.Core.Migrations
{
    /// <summary>
    /// Odpowiada za wypełnienie bazy danych wartościami początkowymi.
    /// </summary>
    public class InitialDataLoader
    {
        private readonly string Admin;
        private readonly string[] RoleNames = { "Administrator", "Student", "Teacher" };
        private string AdminRole
        {
            get { return RoleNames[0]; }
        }

        private User[] Users;
        private User[] Teachers;
        private User[] Students;
        private readonly IDomainContext _context;
        private readonly ApplicationUserManager _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private User _admin;

        /// <summary>
        /// Tworzy nową instancję inicjalizatora
        /// </summary>
        /// <param name="domainContext">Obiekt kontekstu domeny.</param>
        public InitialDataLoader(DomainContext domainContext)
        {
            _context = domainContext;
            _userManager = new ApplicationUserManager(new UserStore<User>(domainContext));
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(domainContext));
            Admin = _userManager.SuperUserLogin;
            Users = new[]
            {
                new User
                {
                    UserName = Admin,
                    Email = "su@su.su",
                    PasswordHash = "su1234", //plain password that will be hashed
                    EmailConfirmed = true
                },
            };
        }

        /// <summary>
        /// Wykonuje inicjalizację bazy za pomocą domyślnych wartości początkowych
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void WithDefault()
        {
            try
            {
                WithRoles();
                WithUsers();
                _admin = _userManager.Users
                .FirstOrDefault(u => u.UserName.Equals(Admin, StringComparison.CurrentCultureIgnoreCase));
                WithAdmin(Admin);
                AddPagesAndMenus();
                WithSubjects();
                WithPhrases();
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                string s = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    s += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:\n",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        s += string.Format("- Property: \"{0}\", Error: \"{1}\"\n",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw new Exception(s);
            }
        }

        private void WithPhrases()
        {
            _context.Phrases.Add(new Phrase { Key = "welcome", CountryCode = "pl", Value = "Witaj!" });
            _context.Phrases.Add(new Phrase { Key = "welcome", CountryCode = "en", Value = "Welcome!" });

            _context.Phrases.Add(new Phrase { Key = "adminPanel", CountryCode = "pl", Value = "Administracja" });
            _context.Phrases.Add(new Phrase { Key = "adminPanel", CountryCode = "en", Value = "Admin panel" });

            _context.Phrases.Add(new Phrase { Key = "logOff", CountryCode = "pl", Value = "Wyloguj" });
            _context.Phrases.Add(new Phrase { Key = "logOff", CountryCode = "en", Value = "Sign Out" });

            _context.Phrases.Add(new Phrase { Key = "logIn", CountryCode = "pl", Value = "Zaloguj" });
            _context.Phrases.Add(new Phrase { Key = "logIn", CountryCode = "en", Value = "Sign In" });

            _context.Phrases.Add(new Phrase { Key = "email", CountryCode = "pl", Value = "Email" });
            _context.Phrases.Add(new Phrase { Key = "email", CountryCode = "en", Value = "Email" });

            _context.Phrases.Add(new Phrase { Key = "password", CountryCode = "pl", Value = "Hasło" });
            _context.Phrases.Add(new Phrase { Key = "password", CountryCode = "en", Value = "Password" });

            _context.Phrases.Add(new Phrase { Key = "teaching", CountryCode = "pl", Value = "Dydaktyka" });
            _context.Phrases.Add(new Phrase { Key = "teaching", CountryCode = "en", Value = "Teaching" });

            _context.Phrases.Add(new Phrase { Key = "home", CountryCode = "pl", Value = "Strona główna" });
            _context.Phrases.Add(new Phrase { Key = "home", CountryCode = "en", Value = "Home" });

            _context.Phrases.Add(new Phrase { Key = "semester", CountryCode = "pl", Value = "Semestr" });
            _context.Phrases.Add(new Phrase { Key = "semester", CountryCode = "en", Value = "Semester" });


            _context.Phrases.Add(new Phrase { Key = "news", CountryCode = "pl", Value = "Aktualności" });
            _context.Phrases.Add(new Phrase { Key = "news", CountryCode = "en", Value = "News" });

            _context.Phrases.Add(new Phrase { Key = "syllabus", CountryCode = "pl", Value = "Sylabus" });
            _context.Phrases.Add(new Phrase { Key = "syllabus", CountryCode = "en", Value = "Syllabus" });

            _context.Phrases.Add(new Phrase { Key = "schedule", CountryCode = "pl", Value = "Plan zajęć" });
            _context.Phrases.Add(new Phrase { Key = "schedule", CountryCode = "en", Value = "Schedule" });

            _context.Phrases.Add(new Phrase { Key = "files", CountryCode = "pl", Value = "Materiały dydaktyczne" });
            _context.Phrases.Add(new Phrase { Key = "files", CountryCode = "en", Value = "Files" });

            _context.Phrases.Add(new Phrase { Key = "forum", CountryCode = "pl", Value = "Forum" });
            _context.Phrases.Add(new Phrase { Key = "forum", CountryCode = "en", Value = "Forum" });


            _context.Phrases.Add(new Phrase { Key = "notFoundTitle", CountryCode = "pl", Value = "Nie znaleziono strony" });
            _context.Phrases.Add(new Phrase { Key = "notFoundTitle", CountryCode = "en", Value = "Not found" });

            _context.Phrases.Add(new Phrase { Key = "notFoundText", CountryCode = "pl", Value = "Strona o podanym adresie nie istnieje." });
            _context.Phrases.Add(new Phrase { Key = "notFoundText", CountryCode = "en", Value = "This page does not exist." });

            _context.Phrases.Add(new Phrase { Key = "mySubjects", CountryCode = "pl", Value = "Moje przedmioty" });
            _context.Phrases.Add(new Phrase { Key = "mySubjects", CountryCode = "en", Value = "My subjects" });

            _context.Phrases.Add(new Phrase { Key = "changePassword", CountryCode = "en", Value = "Change password" });
            _context.Phrases.Add(new Phrase { Key = "changePassword", CountryCode = "pl", Value = "Zmień hasło" });

            _context.Phrases.Add(new Phrase { Key = "oldPassword", CountryCode = "en", Value = "Old password" });
            _context.Phrases.Add(new Phrase { Key = "oldPassword", CountryCode = "pl", Value = "Stare hasło" });

            _context.Phrases.Add(new Phrase { Key = "newPassword", CountryCode = "en", Value = "New password" });
            _context.Phrases.Add(new Phrase { Key = "newPassword", CountryCode = "pl", Value = "Nowe hasło" });

            _context.Phrases.Add(new Phrase { Key = "confirmNewPassword", CountryCode = "en", Value = "Confirm new password" });
            _context.Phrases.Add(new Phrase { Key = "confirmNewPassword", CountryCode = "pl", Value = "Potwierdź nowe hasło" });

            _context.Phrases.Add(new Phrase { Key = "forgotYourPassword", CountryCode = "en", Value = "Forgot your password?" });
            _context.Phrases.Add(new Phrase { Key = "forgotYourPassword", CountryCode = "pl", Value = "Zapomniałeś hasło?" });

            _context.Phrases.Add(new Phrase { Key = "emailLink", CountryCode = "en", Value = "Email Link" });
            _context.Phrases.Add(new Phrase { Key = "emailLink", CountryCode = "pl", Value = "Wyślij" });

            _context.Phrases.Add(new Phrase { Key = "resign", CountryCode = "en", Value = "Resign" });
            _context.Phrases.Add(new Phrase { Key = "resign", CountryCode = "pl", Value = "Zrezygnuj" });

            _context.Phrases.Add(new Phrase { Key = "teacherDuringAcceptation", CountryCode = "en", Value = "Acceptation in progress" });
            _context.Phrases.Add(new Phrase { Key = "teacherDuringAcceptation", CountryCode = "pl", Value = "W trakcie akceptacji" });

            _context.Phrases.Add(new Phrase { Key = "requestRejected", CountryCode = "en", Value = "Request rejected" });
            _context.Phrases.Add(new Phrase { Key = "requestRejected", CountryCode = "pl", Value = "Wniosek odrzucony" });

            _context.Phrases.Add(new Phrase { Key = "submit", CountryCode = "en", Value = "Submit" });
            _context.Phrases.Add(new Phrase { Key = "submit", CountryCode = "pl", Value = "Wyślij" });

            _context.Phrases.Add(new Phrase { Key = "checkEmail", CountryCode = "en", Value = "Please check your email to reset your password." });
            _context.Phrases.Add(new Phrase { Key = "checkEmail", CountryCode = "pl", Value = "Sprawdź skrzynkę email, aby zresetować hasło." });

            _context.Phrases.Add(new Phrase { Key = "resetPasswordLink", CountryCode = "en", Value = "Please reset your password by clicking:" });
            _context.Phrases.Add(new Phrase { Key = "resetPasswordLink", CountryCode = "pl", Value = "Aby zresetować hasło kliknij link:" });

            _context.Phrases.Add(new Phrase { Key = "resetPasswordTitle", CountryCode = "en", Value = "Reset password" });
            _context.Phrases.Add(new Phrase { Key = "resetPasswordTitle", CountryCode = "pl", Value = "Zresetuj hasło" });

            _context.Phrases.Add(new Phrase { Key = "confirmPassword", CountryCode = "en", Value = "Confirm password" });
            _context.Phrases.Add(new Phrase { Key = "confirmPassword", CountryCode = "pl", Value = "Potwierdź hasło" });

            _context.Phrases.Add(new Phrase { Key = "resetPasswordConfirmation", CountryCode = "en", Value = "Reset password confirmation" });
            _context.Phrases.Add(new Phrase { Key = "resetPasswordConfirmation", CountryCode = "pl", Value = "Potwierdzenie zmiany hasła" });

            _context.Phrases.Add(new Phrase { Key = "resetPasswordConfirmationText", CountryCode = "en", Value = "Your password has been reset." });
            _context.Phrases.Add(new Phrase { Key = "resetPasswordConfirmationText", CountryCode = "pl", Value = "Twoje hasło zostało zresetowane." });

            _context.Phrases.Add(new Phrase { Key = "registerLink", CountryCode = "en", Value = "Don't have an account yet? Click here." });
            _context.Phrases.Add(new Phrase { Key = "registerLink", CountryCode = "pl", Value = "Nie masz jeszcze konta? Kliknij tu." });

            _context.Phrases.Add(new Phrase { Key = "register", CountryCode = "en", Value = "New account" });
            _context.Phrases.Add(new Phrase { Key = "register", CountryCode = "pl", Value = "Nowe konto" });

            _context.Phrases.Add(new Phrase { Key = "firstName", CountryCode = "en", Value = "First name" });
            _context.Phrases.Add(new Phrase { Key = "firstName", CountryCode = "pl", Value = "Imię" });

            _context.Phrases.Add(new Phrase { Key = "lastName", CountryCode = "en", Value = "Last name" });
            _context.Phrases.Add(new Phrase { Key = "lastName", CountryCode = "pl", Value = "Nazwisko" });

            _context.Phrases.Add(new Phrase { Key = "indexNumber", CountryCode = "en", Value = "Index number" });
            _context.Phrases.Add(new Phrase { Key = "indexNumber", CountryCode = "pl", Value = "Nr indeksu" });

            _context.Phrases.Add(new Phrase { Key = "personalIdentityNumber", CountryCode = "en", Value = "Personal identity number" });
            _context.Phrases.Add(new Phrase { Key = "personalIdentityNumber", CountryCode = "pl", Value = "Pesel" });





            _context.Phrases.Add(new Phrase { Key = "invalidLoginAttempt", CountryCode = "en", Value = "Invalid login attempt" });
            _context.Phrases.Add(new Phrase { Key = "invalidLoginAttempt", CountryCode = "pl", Value = "Nieudana próba logowania" });


            _context.Phrases.Add(new Phrase { Key = "accountNotActivatedYet", CountryCode = "en", Value = "Account not activatef yet" });
            _context.Phrases.Add(new Phrase { Key = "accountNotActivatedYet", CountryCode = "pl", Value = "Konto nie jest jeszcze aktywne" });


            _context.Phrases.Add(new Phrase { Key = "registeredSuccessfully", CountryCode = "en", Value = "Registered successfully" });
            _context.Phrases.Add(new Phrase { Key = "registeredSuccessfully", CountryCode = "pl", Value = "Zarejestrowano pomyślnie" });

        }

        private void AddPagesAndMenus()
        {
            var kontakt = new PageGroup();
            var badania = new PageGroup();
            var kadra = new PageGroup();

            var telefony = new PageGroup();
            var adresy = new PageGroup();

            var profesorowie = new PageGroup();
            var doktorzy = new PageGroup();
            var magistrzy = new PageGroup();

            var publikacje = new PageGroup();

            var ksiazki = new PageGroup();
            var artykuly = new PageGroup();

            var pl = new Language { Title = "polski", CountryCode = "pl" };
            var en = new Language { Title = "english", CountryCode = "en" };
            var kontaktPage = new Page
            {
                Title = "Kontakt",
                UrlName = "Kontakt",
                Group = kontakt,
                Language = pl,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
            };
            var badaniaPage = new Page
            {
                Title = "Badania",
                UrlName = "Badania",
                Language = pl,
                Group = badania,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
            };
            var kadraPage = new Page
            {
                Title = "Kadra",
                UrlName = "Kadra",
                Language = pl,
                Group = kadra,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
            };
            var publikacjePage = new Page
            {
                Title = "Publikacje",
                UrlName = "Publikacje",
                Language = pl,
                Group = publikacje,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent = badaniaPage
            };
            var telefonyPage = new Page
            {
                Title = "Telefony",
                UrlName = "Telefony",
                Language = pl,
                Group = telefony,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent = kontaktPage
            };
            var magistrzyPage = new Page
            {
                Title = "Magistrzy",
                UrlName = "Magistrzy",
                Language = pl,
                Group = magistrzy,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent = kadraPage
            };
            var adresyPage = new Page
            {
                Title = "Adresy",
                UrlName = "Adresy",
                Language = pl,
                Group = adresy,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent = kontaktPage
            };
            var profesorowiePage = new Page
            {
                Title = "Profesorowie",
                UrlName = "Profesorowie",
                Language = pl,
                Group = profesorowie,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent = kadraPage
            };
            var doktorzyPage = new Page
            {
                Title = "Doktorzy",
                UrlName = "Doktorzy",
                Language = pl,
                Group = doktorzy,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent=kadraPage
            };
            var ksiazkiPage = new Page
            {
                Title = "Książki",
                UrlName = "Ksiazki",
                Language = pl,
                Group = ksiazki,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent = publikacjePage
            };
            var artykulyPage = new Page
            {
                Title = "Artykuły",
                UrlName = "Artykuly",
                Language = pl,
                Group = artykuly,
                CreationDate = DateTime.Now,
                LastUpdateDate = DateTime.Now,
                Parent = publikacjePage
            };
            var pagesPl = new List<Page> {kontaktPage, badaniaPage, kadraPage, magistrzyPage, doktorzyPage, profesorowiePage, adresyPage, telefonyPage, publikacjePage, ksiazkiPage, artykulyPage };

            foreach (var p in pagesPl)
                _context.Pages.Add(p);

            var pagesEn = new List<Page>
            {
                new Page
                {
                    Title = "Contact",
                    Language = en,
                    UrlName = "Contact",
                    Group = kontakt,
                    CreationDate = DateTime.Now,
                    LastUpdateDate = DateTime.Now
                },
                new Page
                {
                    Title = "Research",
                    Language = en,
                    UrlName = "Research",
                    Group = badania,
                    CreationDate = DateTime.Now,
                    LastUpdateDate = DateTime.Now
                },
                new Page
                {
                    Title = "Staff",
                    UrlName = "Staff",
                    Language = en,
                    Group = kadra,
                    CreationDate = DateTime.Now,
                    LastUpdateDate = DateTime.Now
                }
            };
            foreach (var p in pagesEn)
                _context.Pages.Add(p);

            var mainMenu = new MenuGroup { Id = 1 };
            var tileMenu = new MenuGroup { Id = 2 };

            var tileMenuPl = new Menu { Language = pl, Group = tileMenu };
            var tileMenuEn = new Menu { Language = en, Group = tileMenu };

            var menuPl = new Menu { Language = pl, Group = mainMenu };
            var menuEn = new Menu { Language = en, Group = mainMenu };

            menuPl.Items = pagesPl.Take(3).Select((p, i) => new MenuItem { Page = p, Order = i }).ToList();
            menuEn.Items = pagesEn.Select((p, i) => new MenuItem { Page = p, Order = i }).ToList();

            _context.Menus.Add(tileMenuPl);
            _context.Menus.Add(tileMenuEn);

            _context.Menus.Add(menuPl);
            _context.Menus.Add(menuEn);
        }

        private void WithSubjects()
        {
            string[] subjects1 = { "Analiza 1", "Algebra", "Podstawy programowania strukturalnego" };
            string[] subjects1Names = { "analiza-1", "algebra", "podstawy-programowania-strukturalnego" };
            int i = 0;
            foreach (var subject in subjects1)
            {
                var sub = new Subject
                {
                    Name = subject,
                    Semester = 1,
                    UrlName = subjects1Names[i++],
                    Schedule = new Schedule {Content = "content", PublishDate = DateTime.Now, AuthorName = "Admin"},
                    Syllabus = new Syllabus {Content = "content", PublishDate = DateTime.Now, AuthorName = "Admin"},
                    News = new List<News>
                    {
                        new News
                        {
                            Header = "Article 1",
                            Content = "Content",
                            PublishDate = DateTime.Now,
                            AuthorName = "Admin"
                        },
                        new News
                        {
                            Header = "Article 2",
                            Content = "Content",
                            PublishDate = DateTime.Now,
                            AuthorName = "Admin"
                        },
                    },
                };
                sub.Teachers = new Collection<TeacherSubject>
                {
                    new TeacherSubject{Teacher = Teachers[0], Subject = sub},
                    new TeacherSubject{Teacher = Teachers[2], Subject = sub},
                    new TeacherSubject{Teacher = Teachers[3], Subject = sub},
                };
                var req1 = new SignUpRequest { CreateTime = DateTime.Now, Student = Students[0], Subject = sub };
                req1.Approve();
                var req2 = new SignUpRequest { CreateTime = DateTime.Now, Student = Students[2], Subject = sub };
                var req3 = new SignUpRequest { CreateTime = DateTime.Now, Student = Students[4], Subject = sub };
                sub.SignUpRequests = new Collection<SignUpRequest>
                {
                    req1, req2, req3
                };
                _context.Subjects.Add(sub);
            }
            string[] subjects2 = { "Metody Numeryczne 1", "Analiza 2", "Programowanie" };
            string[] subjects2Names = { "metody-numeryczne-1", "analiza-2", "programowanie" };
            i = 0;
            foreach (var subject in subjects2)
            {
                _context.Subjects.Add(new Subject
                {
                    Name = subject,
                    Semester = 2,
                    UrlName = subjects2Names[i++],
                    Schedule = new Schedule { Content = "content", PublishDate = DateTime.Now, AuthorName = "Admin" },
                    Syllabus = new Syllabus { Content = "content", PublishDate = DateTime.Now, AuthorName = "Admin" },
                    News = new List<News>
                    {
                        new News
                        {
                            Header = "Article 1",
                            Content = "Content",
                            PublishDate = DateTime.Now,
                            AuthorName = "Admin"
                        },
                        new News
                        {
                            Header = "Article 2",
                            Content = "Content",
                            PublishDate = DateTime.Now,
                            AuthorName = "Admin"
                        },
                    }
                });
            }
        }

        private void WithRoles()
        {
            foreach (var roleName in RoleNames)
            {
                _roleManager.Create(new IdentityRole(roleName));
            }
        }

        private void WithUsers()
        {
            foreach (var user in Users)
            {
                var result = _userManager.Create(user, user.PasswordHash);
                if (result == IdentityResult.Failed())
                {
                    throw new Exception("Cannot create user");
                }
            }
            Teachers = new[]
            {
                new User
                {
                    UserName = "nauczyciel@polskiego.pl",
                    Email = "nauczyciel@polskiego.pl",
                    PasswordHash = "su1234",
                    FirstName = "Andrzej",
                    LastName = "Kowalski",
                    EmailConfirmed = true
                },
                new User
                {
                    UserName = "nauczyciel@angielskiego.pl",
                    Email = "nauczyciel@angielskiego.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Bruce",
                    LastName = "Willis",
                },
                new User
                {
                    UserName = "nauczyciel@matematyki.pl",
                    Email = "nauczyciel@matematyki.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Roman",
                    LastName = "Całka",
                },
                new User
                {
                    UserName = "nauczyciel@historii.pl",
                    Email = "nauczyciel@historii.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Ireneusz",
                    LastName = "Duda",
                },
                new User
                {
                    UserName = "nauczyciel@biologii.pl",
                    Email = "nauczyciel@biologii.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Maria",
                    LastName = "Jaworska",
                },
            };

            Students = new[]
            {
                new User
                {
                    UserName = "uczen@jurek.pl",
                    Email = "uczen@jurek.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Jerzy",
                    LastName = "Jeż",
                },
                new User
                {
                    UserName = "uczen@marek.pl",
                    Email = "uczen@marek.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Marek",
                    LastName = "Łukasiewicz",
                },
                new User
                {
                    UserName = "uczen@marcin.pl",
                    Email = "uczen@marcin.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Marcin",
                    LastName = "Janowski",
                },
                new User
                {
                    UserName = "uczen@tomek.pl",
                    Email = "uczen@tomek.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Tomasz",
                    LastName = "Nowak",
                },
                new User
                {
                    UserName = "uczen@mietek.pl",
                    Email = "uczen@mietek.pl",
                    PasswordHash = "su1234",
                    EmailConfirmed = true,
                    FirstName = "Mieczysław",
                    LastName = "Dąbrowski",
                },
            };
            foreach (var teacher in Teachers)
            {
                var result = _userManager.Create(teacher, teacher.PasswordHash);
                if (result == IdentityResult.Failed())
                    throw new Exception("Cannot create teacher");
                _userManager.AddToRole(teacher.Id, "Teacher");
            }

            foreach (var student in Students)
            {
                var result = _userManager.Create(student, student.PasswordHash);
                if (result == IdentityResult.Failed())
                    throw new Exception("Cannot create student");
                _userManager.AddToRole(student.Id, "Student");
            }
        }

        private void WithAdmin(string userName)
        {
            if (_admin != null)
            {
                _userManager.AddToRole(_admin.Id, AdminRole);
            }
        }
    }
}