﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web.Mvc;
using MVCForum.Domain.Constants;
using MVCForum.Domain.DomainModel;
using MVCForum.Domain.Interfaces.Services;
using MVCForum.Domain.Interfaces.UnitOfWork;

namespace UniversityWebsite.Forum.Controllers
{
    /// <summary>
    /// Kontroler odpowiedzialny za zarządzanie użytkownikami forum w celu integraci z główną częścią serwisu.
    /// </summary>
    [System.Web.Http.RoutePrefix("sso")]
    public class SsoController : Controller
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IMembershipService _membershipService;
        private readonly ILocalizationService _localizationService;
        private readonly IRoleService _roleService;
        private readonly ISettingsService _settingsService;
        private readonly ILoggingService _loggingService;


        private readonly IBannedEmailService _bannedEmailService;
        private readonly IBannedWordService _bannedWordService;

        /// <summary>
        /// Tworzy nową instancję kontrolera.
        /// </summary>
        /// <param name="loggingService"></param>
        /// <param name="unitOfWorkManager"></param>
        /// <param name="membershipService"></param>
        /// <param name="localizationService"></param>
        /// <param name="roleService"></param>
        /// <param name="settingsService"></param>
        /// <param name="bannedEmailService"></param>
        /// <param name="bannedWordService"></param>
        public SsoController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService, ILocalizationService localizationService,
            IRoleService roleService, ISettingsService settingsService, IBannedEmailService bannedEmailService, IBannedWordService bannedWordService)
        {
            _bannedEmailService = bannedEmailService;
            _bannedWordService = bannedWordService;


            _unitOfWorkManager = unitOfWorkManager;
            _membershipService = membershipService;
            _localizationService = localizationService;
            _roleService = roleService;
            _settingsService = settingsService;
            _loggingService = loggingService;
        }

        private class ResultViewModel
        {
            public ResultViewModel()
            {
                
            }
            public ResultViewModel(bool isSuccess, string message=null)
            {
                Message = message;
                IsSuccess = isSuccess;  
            }
            public string Message { get; set; }
            public bool IsSuccess { get; set; }
        }

        public class CreateUserViewModel
        {
            [Required]
            [StringLength(150, MinimumLength = 4)]
            public string Email { get; set; }
            [Required]
            public bool IsAdmin { get; set; }
        }

        /// <summary>
        /// Dodaje nowego użytkownika do bazy forum.
        /// </summary>
        /// <param name="userModel">Dane noego użytkownika</param>
        /// <returns>Informacja o powodzeniu utworzenia użytkownika.</returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        public JsonResult Register(CreateUserViewModel userModel)
        {
            if (!ModelState.IsValid)
                throw new Exception("Model invalid");

            try
            {
                if (_settingsService.GetSettings().SuspendRegistration != true)
                {
                    using (_unitOfWorkManager.NewUnitOfWork())
                    {
                        // Secondly see if the email is banned
                        if (_bannedEmailService.EmailIsBanned(userModel.Email))
                        {
                            return
                                Json(
                                    new ResultViewModel
                                    {
                                        IsSuccess = false,
                                        Message = _localizationService.GetResourceString("Error.EmailIsBanned")
                                    });
                        }
                    }

                    // Do the register logic
                    return MemberRegisterLogic(userModel);

                }
                return Json(new ResultViewModel { IsSuccess = false, Message = "" });
            }
            catch (Exception ex)
            {
                return Json(new ResultViewModel { IsSuccess = false, Message = ex.Message });
            }
        }

        private JsonResult MemberRegisterLogic(CreateUserViewModel userModel)
        {
            using (var unitOfWork = _unitOfWorkManager.NewUnitOfWork())
            {
                var userToSave = new MembershipUser
                {
                    UserName = _bannedWordService.SanitiseBannedWords(userModel.Email),
                    Email = userModel.Email,
                    Password = "Q1@wE3$rT5^yU7*i",
                    IsApproved = true,
                    Comment = string.Empty,
                };

                // Now check settings, see if users need to be manually authorised
                // OR Does the user need to confirm their email
                var manuallyAuthoriseMembers = _settingsService.GetSettings().ManuallyAuthoriseNewMembers;
                var memberEmailAuthorisationNeeded = _settingsService.GetSettings().NewMemberEmailConfirmation ?? false;
                if (manuallyAuthoriseMembers || memberEmailAuthorisationNeeded)
                {
                    userToSave.IsApproved = false;
                }

                var createStatus = _membershipService.CreateUser(userToSave);

                if (createStatus != MembershipCreateStatus.Success)
                {
                    ModelState.AddModelError(string.Empty, _membershipService.ErrorCodeToString(createStatus));
                }
                else
                {
                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.Rollback();
                        _loggingService.Error(ex);
                        return Json(new ResultViewModel { IsSuccess = false, Message = ex.ToString() });
                    }
                }
            }
            using (var unitOfWork = _unitOfWorkManager.NewUnitOfWork())
            {
                if (userModel.IsAdmin)
                {
                    var user = _membershipService.GetUserByEmail(userModel.Email);
                    var role = _roleService.GetRole(AppConstants.AdminRoleName);
                    user.Roles = new List<MembershipRole> { role };
                }
                try
                {
                    unitOfWork.Commit();
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    _loggingService.Error(ex);
                    return Json(new ResultViewModel {IsSuccess = false, Message = ex.ToString()});
                }
            }
            return Json(new ResultViewModel { IsSuccess = true });
        }

        public class EditUserViewModel
        {
            [Required]
            public string OldEmail { get; set; }
            [Required]
            [StringLength(150, MinimumLength = 4)]
            public string NewEmail { get; set; }
            [Required]
            public bool IsAdmin { get; set; }
        }

        /// <summary>
        /// Nadpisuje dane użytkownika.
        /// </summary>
        /// <param name="userModel">Dane do nadpisania.</param>
        /// <returns>Informacja o powodzeniu edycji.</returns>
        /// <exception cref="Exception"></exception>
        /// <exception cref="ApplicationException"></exception>
        [HttpPost]
        public JsonResult Edit(EditUserViewModel userModel)
        {
            if (!ModelState.IsValid)
                throw new Exception("Model invalid");

            try
            {
                using (var unitOfWork = _unitOfWorkManager.NewUnitOfWork())
                {
                    var user = _membershipService.GetUserByEmail(userModel.OldEmail);
                    if (user == null)
                    {
                        throw new ApplicationException("Cannot edit user - user does not exist");
                    }
                    if (userModel.OldEmail != userModel.NewEmail)
                    {
                        user.Email = userModel.NewEmail;
                        user.UserName = userModel.NewEmail;
                    }
                    MembershipRole role = _roleService.GetRole(userModel.IsAdmin ? AppConstants.AdminRoleName : "Standard Members");
                    user.Roles = new List<MembershipRole> { role };
                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.Rollback();
                        _loggingService.Error(ex);
                        return Json(new ResultViewModel { IsSuccess = false, Message = ex.ToString() });
                    }
                }
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
                return Json(new ResultViewModel { IsSuccess = false, Message = ex.ToString() });
            }
            return Json(new ResultViewModel { IsSuccess = true });
        }
        public class DeleteUserViewModel
        {
            [Required]
            public string Email { get; set; }
        }

        /// <summary>
        /// Usuwa użytkownika z bazy forum.
        /// </summary>
        /// <param name="userModel">Obiekt zawierający email użytkownik do usunięcia.</param>
        /// <returns>Informacja o powodzeniu usunięcia.</returns>
        /// <exception cref="Exception"></exception>
        /// <exception cref="ApplicationException"></exception>
        [HttpPost]
        public JsonResult Remove(DeleteUserViewModel userModel)
        {
            if(!ModelState.IsValid)
                throw new Exception("Model invalid");
            try
            {
                using (var unitOfWork = _unitOfWorkManager.NewUnitOfWork())
                {
                    var user = _membershipService.GetUserByEmail(userModel.Email);
                    if (user == null)
                    {
                        throw new ApplicationException("Cannot delete user - user does not exist");
                    }
                    _membershipService.Delete(user, unitOfWork);
                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.Rollback();
                        _loggingService.Error(ex);
                        return Json(new ResultViewModel { IsSuccess = false, Message = ex.Message });
                    }
                }
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
                return Json(new ResultViewModel { IsSuccess = false, Message = ex.Message });
            }
            return Json(new ResultViewModel { IsSuccess = true });
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception e = filterContext.Exception;
            //Log Exception e
            filterContext.ExceptionHandled = true;
            filterContext.Result = Json(new ResultViewModel { IsSuccess = false, Message = e.ToString() });
        }
        //protected override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    base.OnAuthorization(filterContext);

        //    if (!User.Identity.IsAuthenticated)
        //        filterContext.Result = Json(new { IsSuccess = false, Message = "Access denied." });
        //}
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (String.IsNullOrEmpty(filterContext.HttpContext.Request.Headers["Authorization"]))
                {
                    filterContext.Result = new HttpUnauthorizedResult();
                }
                else
                {
                    if (filterContext.HttpContext.Request.Headers["Authorization"].StartsWith("Basic ", StringComparison.InvariantCultureIgnoreCase))
                    {
                        string[] credentials = Encoding.ASCII.GetString(Convert.FromBase64String(filterContext.HttpContext.Request.Headers["Authorization"].Substring(6))).Split(':');

                        if (credentials.Length == 2)
                        {
                            if (String.IsNullOrEmpty(credentials[0]))
                            {
                                filterContext.Result = new HttpUnauthorizedResult();
                            }
                            else if (!(credentials[0] == "UniversityWebsite" && credentials[1] == "0d5d5345-32d7-4173-8d7c-ec137e0ba4e9"))
                            {
                                filterContext.Result = new HttpUnauthorizedResult();
                            }
                        }
                        else
                        {
                            filterContext.Result = new HttpUnauthorizedResult();
                        }
                    }
                    else
                    {
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                }

                base.OnActionExecuting(filterContext);
            }
            catch
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
    }
}