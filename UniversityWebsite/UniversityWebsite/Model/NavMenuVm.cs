﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityWebsite.Model.Page;

namespace UniversityWebsite.Model
{
    public class NavMenuVm
    {
        public NavMenuVm()
        {
           Items = new List<MenuPageItemVm>();
        }
        public List<MenuPageItemVm> Items { get; set; }
        public bool IsTopLevel { get; set; }
        public bool IsHigherLevel { get; set; }
    }
}