﻿using System.Collections.Generic;

namespace UniversityWebsite.Model.Page
{
    public class NavMenuItemVm
    {
        public NavMenuItemVm()
        {
            Items = new List<NavMenuItemVm>();
        }
        public string Title { get; set; }
        public string UrlName { get; set; }
        public List<NavMenuItemVm> Items { get; set; }
    }
}