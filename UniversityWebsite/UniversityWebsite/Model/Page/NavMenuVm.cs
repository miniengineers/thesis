﻿using System.Collections.Generic;
using UniversityWebsite.Services.Model;

namespace UniversityWebsite.Model.Page
{
    public class NavMenuVm
    {
        public PageNavigationMenu PageNavMenu { get; set; }
        public List<List<TeachingMenuItem>> TeachingNavMenu { get; set; }
        //public NavMenuVm()
        //{
        //   PageItems = new List<NavMenuItemVm>();
        //}
        //public NavMenuItemVm TeachingItem { get; set; }
        //public List<NavMenuItemVm> PageItems { get; set; }
    }
}