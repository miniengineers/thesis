﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using UniversityWebsite.Core;
using UniversityWebsite.Filters;
using UniversityWebsite.Services;
using UniversityWebsite.ViewModels;

namespace UniversityWebsite.Controllers
{
    /// <summary>
    /// Kontroler odpowiedzialny za zarządzanie zawartością konta użytkownika.
    /// </summary>
    [Authorize]
    [MainMenu]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly IDictionaryService _dictionaryService;

        /// <summary>
        /// Tworzy instancję kontrolera
        /// </summary>
        /// <param name="dictionaryService">Serwis odpowiedzialny za tłumaczenia fraz systemu.</param>
        public ManageController(IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService;
        }

        /// <summary>
        /// Tworzy instancję kontrolera
        /// </summary>
        /// <param name="userManager">Manager zarządzający bezpośrednio użytkownikami systemu.</param>
        /// <param name="signInManager">Manager zarządzający autentykacją w systemie</param>
        /// <param name="dictionaryService">Serwis odpowiedzialny za tłumaczenia fraz systemu.</param>
        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IDictionaryService dictionaryService)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            _dictionaryService = dictionaryService;
        }

        private ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            set
            {
                _signInManager = value;
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// Zwraca widok zmiany hasła użytkownika.
        /// </summary>
        /// <returns>Obiekt widoku.</returns>
        public ActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// Zmienia hasło zalogowanego użytkownika.
        /// </summary>
        /// <param name="model">Dane do zmiany hasła</param>
        /// <returns>Obiekt widoku zmienionego hasła</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    if (user.HasForumAccount)
                        FormsAuthentication.SetAuthCookie(user.Email, false);
                }
                return RedirectToAction("Index", "Home", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        /// <summary>
        /// Status powodzenia akcji kontrolera Manage.
        /// </summary>
        private enum ManageMessageId
        {
            //AddPhoneSuccess,
            /// <summary>
            /// Pomyślnie zmieniono hasło.
            /// </summary>
            ChangePasswordSuccess,
            //SetTwoFactorSuccess,
            //SetPasswordSuccess,
            //RemoveLoginSuccess,
            //RemovePhoneSuccess,
            //Error
        }

        #endregion
    }

}