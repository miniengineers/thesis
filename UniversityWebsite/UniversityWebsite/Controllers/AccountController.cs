﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.WebPages;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using UniversityWebsite.Core;
using UniversityWebsite.Domain.Model;
using UniversityWebsite.Filters;
using UniversityWebsite.Model;
using UniversityWebsite.Services;
using UniversityWebsite.ViewModels;

namespace UniversityWebsite.Controllers
{
    /// <summary>
    /// Kontroler odpowiedzialny za operacje związane z kontem użytkownika.
    /// </summary>
    [MainMenu]
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IDictionaryService _dictionaryService;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        /// <summary>
        /// Tworzy nową instancję kontrolera.
        /// </summary>
        /// <param name="userManager">Manager zarządzający bezpośrednio użytkownikami systemu.</param>
        /// <param name="signInManager">Manager zarządzający autentykacją w systemie</param>
        /// <param name="dictionaryService">Serwis odpowiedzialny za tłumaczenia fraz.</param>
        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService;
            UserManager = userManager;
            SignInManager = signInManager;
        }

        private ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            set
            {
                _signInManager = value;
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        /// <summary>
        /// Zwraca widok logowania do systemu.
        /// </summary>
        /// <param name="returnUrl">URL, na który następuje przekierowanie po pomyślnym zalogowaniu.</param>
        /// <returns>Widok strony logowania</returns>
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = "/Home";
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        /// <summary>
        /// Dokonuje uwierzytelnienia użytkownika w systemie.
        /// </summary>
        /// <param name="model">Dane logowania</param>
        /// <param name="returnUrl">URL, na który następuje przekierowanie po pomyślnym zalogowaniu.</param>
        /// <returns>Widok strony głównej, lub obiekt przekierowania, jeżeli zostało zdefiniowane</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = UserManager.FindByName(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("", GetPhrase("invalidLoginAttempt"));
                return View(model);
            }
            if (!user.EmailConfirmed)
            {
                ModelState.AddModelError("", GetPhrase("accountNotActivatedYet."));
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: true);
            switch (result)
            {
                case SignInStatus.Success:
                {
                    if (user.HasForumAccount)
                        FormsAuthentication.SetAuthCookie(user.Email, false);

                    if (returnUrl.IsEmpty()) return RedirectToAction("Index", "Home");
                    return RedirectToLocal(returnUrl);
                }
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.LockedOut:
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", GetPhrase("invalidLoginAttempt"));
                    return View(model);
            }
        }

        private void SignUserOutForum()
        {
            //if (!User.Identity.IsAuthenticated) return;
            //var id = User.Identity.GetUserId();
            //var user = UserManager.FindById(id);
            //if (user == null)
            //    return;
            //    //throw new Exception("User with id: " + id + " does not exist.");
            //if (user.HasForumAccount)
                FormsAuthentication.SignOut();
        }
        
        /// <summary>
        /// Zwraca widok formularza rejestracji użytkownika.
        /// </summary>
        /// <returns>Obiekt widoku.</returns>
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        
        /// <summary>
        /// Rejestruje użytkownika w systemie.
        /// </summary>
        /// <param name="model">Dane użytkownika</param>
        /// <returns>Widok potwierdzenia rejestracji.</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Pesel = model.Pesel,
                    IndexNumber = model.IndexNumber
                };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                    return RedirectToAction("Index", "Home", new { Message = GetPhrase("registeredSuccessfully") });
                AddErrors(result);
            }
            return View(model);
        }

        /// <summary>
        /// Zwraca widok zawierający formularz służący do podania adresu email w celu zreseowania hasła.
        /// </summary>
        /// <returns>Obiekt widoku.</returns>
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        
        /// <summary>
        /// Wysyła email na podany adres z tokenem pozwalającym zresetować hasło.
        /// </summary>
        /// <param name="model">Obiekt zawierający email użytkownika.</param>
        /// <returns>Obiekt widoku.</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null )
                    return View("ForgotPasswordConfirmation");

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, GetPhrase("resetPasswordTitle"),
                    string.Format("{0} {1}", GetPhrase("resetPasswordLink"), callbackUrl));
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private string GetPhrase(string key)
        {
            return _dictionaryService.GetTranslationCached(key, (string)Session[Consts.SessionKeyLang]);
        }

        /// <summary>
        /// Zwraca widok potwierdzający wysłanie wniosku o reset hasła.
        /// </summary>
        /// <returns>Obiekt widoku.</returns>
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        
        /// <summary>
        /// Zwraca eidok potwierdzający zmianę zapomnianego hasła.
        /// </summary>
        /// <param name="code">Kod mówiący o powodzeniu operacji.</param>
        /// <returns>Obiekt widoku.</returns>
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        
        /// <summary>
        /// Zmienia hasło użytkownika na nowe.
        /// </summary>
        /// <param name="model">Obiekt zawierający nowe hasło użytkownika.</param>
        /// <returns>Obiekt widoku</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        /// <summary>
        /// Zwraca eidok potwierdzający zmianę zapomnianego hasła.
        /// </summary>
        /// <returns>Obiekt widoku.</returns>
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// Wylogowuje użytkownika z systemu.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            SignUserOutForum();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }
        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Page");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            private string LoginProvider { get; set; }
            private string RedirectUri { get; set; }
            private string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }

}