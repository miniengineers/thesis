﻿using System.ComponentModel.DataAnnotations;
using UniversityWebsite.Services.Validation;

namespace UniversityWebsite.Api.Model.Teaching
{
    public class SubjectPost
    {
        [Required, StringLength(64)]
        [RegularExpression(Consts.UrlRegex, ErrorMessage = "Invalid characters")]
        public string Name { get; set; }

        [Required, Range(1,10)]
        public int Semester { get; set; }

        public Article Syllabus { get; set; }

        public Article Schedule { get; set; }
    }
}