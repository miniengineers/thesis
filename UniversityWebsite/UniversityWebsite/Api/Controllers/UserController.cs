﻿using System.Collections.Generic;
using System.Web.Http;
using UniversityWebsite.Api.Model;
using UniversityWebsite.Api.Model.Users;
using Microsoft.AspNet.Identity;
using UniversityWebsite.Filters;
using UniversityWebsite.Helper;
using UniversityWebsite.Services;
using UniversityWebsite.Services.Exceptions;
using UniversityWebsite.Services.Model;

namespace UniversityWebsite.Api.Controllers
{
    /// <summary>
    /// Kontroler odpowiedzialny za zarządzanie użytkownikami systemu.
    /// </summary>
    [RoutePrefix("api/users")]
    [Authorize(Roles = Consts.AdministratorRole)]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Tworzy nową instancję kontrolera.
        /// </summary>
        /// <param name="userService">Serwis odpowiedzialny za zarządzanie użytkownikami systemu.</param>
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Pobiera użytkowników systemu o danej roli.
        /// </summary>
        /// <param name="role">Rola użytkowników</param>
        /// <param name="limit">Maksymalna liczba zwróconych obiektów</param>
        /// <param name="offset">Numer porządkowy pierwszego zwróconego obiektu</param>
        /// <returns>Zbiór obiektów reprezentujących użytkowników systemu.</returns>
        [Route("")]
        [Limit(50), Offset]
        [AntiForgeryValidate]
        public PaginationVm<UserDto> GetUsers(string role = null, int limit = 50, int offset = 0, bool? usersConfirmed = null)
        {
            IEnumerable<UserDto> users = role == null ?
                _userService.GetUsers(limit, offset, usersConfirmed) :
                _userService.GetUsersByRole(role, limit, offset);

            int number = role == null ?
                _userService.GetUsersNumber() :
                _userService.GetUsersNumberByRole(role);

            return new PaginationVm<UserDto>(users, number, limit, offset);
        }

        /// <summary>
        /// Pobiera użytkownika systemu o danym Id.
        /// </summary>
        /// <param name="userId">Id użytkownika</param>
        /// <returns>Obiekt reprezentujący użytkownika systemu.</returns>
        [Route("{userId:guid}")]
        [AntiForgeryValidate]
        public IHttpActionResult GetUser(string userId)
        {
            var user = _userService.GetUser(userId);
            if (user == null)
                return NotFound();
            return Ok(user);
        }
        /// <summary>
        /// Dodaje użytkownika do systemu.
        /// </summary>
        /// <param name="model">Dane  nowego użytkownika</param>
        /// <returns>Obiekt nowego użytkownika.</returns>
        [Route("")]
        [AntiForgeryValidate]
        public IHttpActionResult PostUser(PostUserVm model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userDto = new UserDto
            {
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                IndexNumber = model.IndexNumber,
                Pesel = model.Pesel,
                Role = model.Role
            };
            try
            {
                var user = _userService.CreateUser(userDto);
                return Ok(user);
            }
            catch (IdentityOperationFailedException ex)
            {
                return GetErrorResult(ex.IdentityResult);
            }
        }
        /// <summary>
        /// Nadpisuje dane użytkownika systemu.
        /// </summary>
        /// <param name="userId">Id użytkownika</param>
        /// <param name="model">Dane użytkownika</param>
        /// <returns>Obiekt użytkownika po nadpisaniu.</returns>
        [Route("{userId:guid}")]
        [AntiForgeryValidate]
        public IHttpActionResult PutUser(string userId, PutUserVm model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            if (userId != model.Id)
                return BadRequest("Ids do not match.");
            try
            {
                var userDto = new UserDto
                {
                    Id = model.Id,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    IndexNumber = model.IndexNumber,
                    Pesel = model.Pesel,
                    Role = model.Role
                };
                var user = _userService.UpdateUser(userDto);
                return Ok(user);
            }
            catch (IdentityOperationFailedException ex)
            {
                return GetErrorResult(ex.IdentityResult);
            }
        }

        ///// <summary>
        ///// Usuwa użytkownika z systemu.
        ///// </summary>
        ///// <param name="userId">Id użytkownika</param>
        ///// <returns>Status Http</returns>
        //[Route("{userId:guid}")]
        //public IHttpActionResult DeleteUser(string userId)
        //{
        //    try
        //    {
        //        _userService.DeleteUser(userId);
        //        return Ok();
        //    }
        //    catch (IdentityOperationFailedException ex)
        //    {
        //        return GetErrorResult(ex.IdentityResult);
        //    }
        //}

        /// <summary>
        /// Wyłącza konto użytkowników
        /// </summary>
        /// <param name="userIds">Lista id użytkowników</param>
        /// <returns>Status HTTP</returns>
        [Route("disable")]
        [HttpPost]
        [AntiForgeryValidate]
        public IHttpActionResult DisableUser(string[] userIds)
        {
            try
            {
                var user = _userService.DisableUser(userIds[0]);
                return Ok(user);
            }
            catch (IdentityOperationFailedException ex)
            {
                return GetErrorResult(ex.IdentityResult);
            }
        }

        /// <summary>
        /// Odblokowuje konto użytkownika
        /// </summary>
        /// <param name="userIds">Id użytkownika</param>
        /// <returns>Status HTTP</returns>
        [Route("activate")]
        [HttpPost]
        [AntiForgeryValidate]
        public IHttpActionResult ActivateUser(string[] userIds)
        {
            try
            {
                var user = _userService.ActivateUser(userIds[0]);
                return Ok(user);
            }
            catch (IdentityOperationFailedException ex)
            {
                return GetErrorResult(ex.IdentityResult);
            }
        }
        /// <summary>
        /// Aktywuje konto użytkownika
        /// </summary>
        /// <param name="userId">Id użytkownika</param>
        /// <returns>Status HTTP</returns>
        [Route("confirm")]
        [HttpPost]
        //[AntiForgeryValidate]
        public IHttpActionResult ConfirmUser([FromBody]string userId)
        {
            try
            {
                var user = _userService.ConfirmUser(userId);
                return Ok(user);
            }
            catch (IdentityOperationFailedException ex)
            {
                return GetErrorResult(ex.IdentityResult);
            }
        }
        /// <summary>
        /// Pobiera użytkowników systemu iepotwierdzonych przez administratora.
        /// </summary>
        /// <param name="limit">Maksymalna liczba zwróconych obiektów</param>
        /// <param name="offset">Numer porządkowy pierwszego zwróconego obiektu</param>
        /// <returns>Zbiór obiektów reprezentujących użytkowników systemu.</returns>
        [Route("notconfirmed")]
        [HttpGet]
        //[AntiForgeryValidate]
        [Limit(50),Offset]
        public PaginationVm<UserDto> GetNotConfirmedUsers(int limit = 50, int offset = 0)
        {
            IEnumerable<UserDto> users = _userService.GetNotConfirmedUsers(limit, offset);

            int number = _userService.GetNotConfirmedUsersNumber();

            return new PaginationVm<UserDto>(users, number, limit, offset);
        }
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
                return InternalServerError();

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                    return BadRequest();

                return BadRequest(ModelState);
            }

            return null;
        }


    }

}