﻿'use strict';

angular.module('configApp.pages')

.controller('userEditCtrl', ['$scope', '$stateParams', '$state', 'utils', 'selectedUser', 'userService', 'notificationFactory',
    function ($scope, $stateParams, $state, utils, selectedUser, userService, notificationFactory) {

        $scope.alerts = [];
        $scope.notificationService = notificationFactory($scope.alerts);

        $scope.roles = ["Administrator", "Student", "Teacher"];
        $scope.user = selectedUser;
        if ($scope.user.Password != null) {
            var alert = {
                type: 'success',
                msg: 'User ' + $scope.user.FirstName + " " + $scope.user.LastName + ' has been added. \n GENERATED PASSWORD: ' + $scope.user.Password,
                timeoutDisabled: true
            };
            $scope.notificationService.addAlert(alert);
        }
        $scope.changePassword = ($stateParams.userId == "newUser");

        $scope.update = function () {
            $scope.notificationService.clearNotifications();
            if ($scope.userForm.$valid) {
                if ($stateParams.userId == "newUser") {
                    $scope.user = $scope.user || new Object();
                    userService.post($scope.user, function (response) {
                        $scope.user = response;
                        $scope.users.push($scope.user);
                        $state.go("users.edit", { userId: $scope.user.Id, user: $scope.user });
                    }, $scope.notificationService.handleModelStateErrors);
                } else {
                    userService.update({ id: $scope.user.Id }, $scope.user, function (response) {
                        $scope.user = response;
                        var alert = { type: 'success', msg: 'User ' + $scope.user.FirstName + " " + $scope.user.LastName + ' has been updated.' };
                        $scope.notificationService.addAlert(alert);
                    }, $scope.notificationService.handleModelStateErrors);
                }
            } else {
                utils.showValidation($scope.userForm);
            }
        };

        $scope.disable = function () {
            userService.disableUser($scope.user.Id).then(function (resp) {
                var alert = { type: 'success', msg: 'User ' + $scope.user.FirstName + " " + $scope.user.LastName + ' has been disabled.' };
                $scope.notificationService.addAlert(alert);
                $scope.user = resp.data;
            }, $scope.notificationService.handleModelStateErrors);
        };

        $scope.activ = function () {
            userService.activateUser($scope.user.Id).then(function (resp) {
                $scope.user = resp.data;
                var alert = {
                    type: 'success',
                    msg: 'User ' + $scope.user.FirstName + " " + $scope.user.LastName + ' has been activated. \n GENERATED PASSWORD: ' + $scope.user.Password,
                    timeoutDisabled: true
                };
               $scope.notificationService.addAlert(alert);
            }, $scope.notificationService.handleModelStateErrors);
        };

        $scope.confirm = function() {
            userService.confirmUser($scope.user.Id).then(function (resp) {
                $scope.user = resp.data;
                var alert = {
                    type: 'success',
                    msg: 'User ' + $scope.user.FirstName + " " + $scope.user.LastName + ' has been confirmed.'
                };
                $scope.notificationService.addAlert(alert);
                $scope.$parent.refreshUsers();
            }, $scope.notificationService.handleModelStateErrors);
        };
    }])