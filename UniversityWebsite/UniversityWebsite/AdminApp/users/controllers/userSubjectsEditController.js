﻿
angular.module('configApp.pages')

.controller('userSubjectsEditCtrl', ['$scope', '$stateParams', 'utils', function ($scope, $stateParams, utils) {

    $scope.user = utils.findById($scope.users, $stateParams.userId);

    $scope.update = function () {
        $scope.errors = [];
    };

    $scope.removeOwnedSubject = function(subject) {
        utils.remove($scope.user.OwnedSubjects, subject);
    };

    $scope.removeParticipatedSubject = function (subject) {
        utils.remove($scope.user.ParticipatedSubjects, subject);
    };

    var errorHandler = function () {
        $scope.errors = utils.parseErrors(response.data.ModelState);
    };
}])