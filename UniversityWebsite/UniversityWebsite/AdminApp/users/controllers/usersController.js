﻿angular.module('configApp.users')

.controller('usersCtrl', ['$scope', '$state', '$location', 'userService', function ($scope, $state, $location, userService) {
    $scope.totalUsers = 0;
    $scope.currentPage = 1;
    $scope.usersPerPage = 8;
    $scope.userStatuses = ['all', 'confirmed', 'not-confirmed'];
    $scope.userStatus = $scope.userStatuses[0];
    var userConfirmed = null;

    $scope.addUser = function () {
        $state.go("users.edit", { userId: "newUser", user: { Id: null } });
    };

    getPage(1);

    $scope.pageChanged = function (newPage) {
        getPage(newPage);
    };

    $scope.pagination = {
        current: 1
    };

    $scope.refreshUsers = function() {
        getPage(1);
    };

    function getPage(pageNumber) {
        var offset = (pageNumber - 1) * $scope.usersPerPage;
        userService.queryUsers(null, $scope.usersPerPage, offset, userConfirmed).then(function (response) {
            $scope.totalUsers = response.data.Number;
            $scope.users = response.data.Elements;
        });
    };

    $scope.updateList = function () {
        console.log($scope.userStatus);
        switch ($scope.userStatus) {
            case 'all':
                userConfirmed = null;
                break;
            case 'confirmed':
                userConfirmed = true;
                break;
            case 'not-confirmed':
                userConfirmed = false;
                break;
            default:
                userConfirmed = null;
        }
        getPage(1);
    };
}])