﻿angular.module('configApp.users', ['ui.router', 'configApp.users.service'])
    .factory('usersPost', function ($resource) {
        return $resource('/api/users/:id');
    })
    .config(
    [
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('users', {
                    url: '/users',
                    templateUrl: 'adminapp/views/users/users.html',
                    controller: 'usersCtrl',
                    data: { auth: "admin" }
                })
                .state('users.edit', {
                    url: '/:userId',
                    params: {
                        user: null
                    },
                    data: { auth: "" },
                    views: {
                        '': {
                            templateUrl: 'adminapp/views/users/user.edit.html',
                            data: { auth: "admin" },
                            controller: 'userEditCtrl',
                            resolve: {
                                selectedUser: [
                                    'userService', '$stateParams',
                                    function (userService, $stateParams) {
                                        if ($stateParams.user != null && $stateParams.user.Id == null) {
                                            return $stateParams.user;
                                        }
                                        return userService.findUser($stateParams.userId).then(function (response) {
                                            if ($stateParams.user != null) {
                                                response.Password = $stateParams.user.Password;
                                            };
                                            return response;
                                        }, function (error) {
                                            console.log(error);
                                            return null;
                                        });
                                    }]
                            },
                        },
                    }
                });
        }
    ]);