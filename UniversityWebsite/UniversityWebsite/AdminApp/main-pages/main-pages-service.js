﻿angular.module('configApp.main-pages.service', [
])

.factory('MainPages', ['$resource', function ($resource) {
    return $resource('api/tile/:lang', {}, {
        query: { method: 'GET', isArray: true},
        post: { method: 'POST' },
        update: { method: 'PUT' },
        remove: { method: 'DELETE' }
    });
}])

.factory('mainPagesService', ['$http', 'utils', 'MainPages', function ($http, utils, MainPages) {
    var path = "/api/tile";

    var menus = $http.get(path).then(function (resp) {
        return resp.data;
    });

    var factory = {};
    factory.all = function () { 
        return menus;
    };

    factory.get = function (lang) {
        return menus.then(function() {
            return utils.findByCountryCode(menus, lang);
        });
    };

    factory.update = function(menu) {
        return MainPages.update({ lang: menu.CountryCode }, menu).$promise;
    };


    return factory;
}]);