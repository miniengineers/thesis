﻿angular.module('configApp.main-pages')

.controller('mainPagesEditCtrl', ['$scope', '$state', '$modal', 'menus', 'utils', 'mainPagesService', 'notificationFactory',
    function ($scope, $state, $modal, menus, utils, mainPagesService, notificationFactory) {
        $scope.menus = menus;
        $scope.alerts = [];
        $scope.dirty = false;
        $scope.notificationService = notificationFactory($scope.alerts);

        $scope.activeMenu = menus[0];

        $scope.leftBar = { name: 'pages', url: 'adminapp/views/partials/pages.list.html' };
        $scope.menuEdit = { name: 'menuEdit', url: 'adminapp/views/main-pages/tile.edit.html' };
        $scope.menuList = { name: 'menuList', url: 'adminapp/views/partials/menu.list.html' };

        $scope.sortItems = function (items) {
            return items.sort(function (first, second) {
                return first.Order - second.Order;
            });
        },

        $scope.addToMenu = function (page) {
            if (Enumerable.From($scope.activeMenu.Items).Any(function (item) { return item.PageId == page.Id; })) {
                return;
            }
            $scope.activeMenu.dirty = true;
            var menuItem = new Object();
            menuItem.Order = $scope.activeMenu.Items.length;
            menuItem.Title = page.Title;
            menuItem.PageId = page.Id;

            $scope.activeMenu.Items.push(menuItem);
        },

        $scope.moveUp = function (menuItem, items) {
            $scope.activeMenu.dirty = true;
            items = $scope.sortItems(items);
            menuItem.Order--;
            items[menuItem.Order].Order++;
        },

        $scope.moveDown = function (menuItem, items) {
            $scope.activeMenu.dirty = true;
            items = items.sort(function (first, second) {
                return first.Order - second.Order;
            });
            menuItem.Order++;
            items[menuItem.Order].Order--;
        },

        $scope.remove = function (menuItem, items) {
            $scope.activeMenu.dirty = true;
            items = $scope.sortItems(items);
            utils.remove(items, menuItem);
            for (var i = 0; i < items.length; i++) {
                items[i].Order = i;
            }
        },

        $scope.update = function (menu) {
            $scope.notificationService.clearNotifications();
            mainPagesService.update(menu).then(function (resp) {
                var alert = { type: 'success', msg: 'The ' + menu.CountryCode + ' menu has been updated.' };
                $scope.notificationService.addAlert(alert);
                $scope.activeMenu.dirty = false;
            }, $scope.notificationService.handleModelStateErrors);
        },

        $scope.removeAll = function () {
            $scope.activeMenu.dirty = true;
            $scope.activeMenu.Items = [];
        },

        $scope.menuChanged = function (menu) {
            $scope.activeMenu = menu;
        },

        $scope.pagesFilterExpression = function (menu) {
            return menu.CountryCode == $scope.activeMenu.CountryCode;
        },


        $scope.openGallery = function (menu) {
            var modalInstance = $modal.open({
                templateUrl: 'adminapp/views/gallery/gallery.modal.html',
                controller: 'modalCtrl',
            });

            modalInstance.result.then(function (selectedImageUrl) {
                $scope.activeMenu.dirty = true;
                menu.ImageUrl = selectedImageUrl;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }])