﻿angular.module('configApp.main-pages', ['ui.router', 'configApp.main-pages.service', 'configApp.pages.service'])

    .config(
    [
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('main-pages', {
                    url: '/main-pages',
                    templateUrl: 'adminapp/views/main-pages/main-pages.html',
                    resolve: {
                        menus: ['mainPagesService', function(mainPagesService) {
                            return mainPagesService.all();
                        }],
                    },
                    data: { auth: "admin"},
                    controller: 'mainPagesEditCtrl'
                });
        }
    ]);