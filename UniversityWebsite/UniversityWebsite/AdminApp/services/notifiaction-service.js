﻿angular.module('configApp.notification.service', [

])

.factory('notificationFactory',['utils', function notificationFactory(utils) {
    return function(notifications) {
        return new NotificationService(notifications, utils);
    };
}]);

function NotificationService(notifications, utils) {

    this.closeAlert = function (index) {
        notifications.splice(index, 1);
    };

    this.addAlert = addAlert;

    function addAlert(alert) {
        notifications.push(alert);
    };

    this.clearNotifications = function () {
        notifications.splice(0, notifications.length);
    };

    this.handleModelStateErrors = function(response) {
        var errors = utils.parseErrors(response.data.ModelState);
        if (errors.length < 1) {
            var alert = { type: 'alert', msg: 'Error: unexpected server error' };
            addAlert(alert);
        }
        for (var i = 0; i < errors.length; i++) {
            var alert = { type: 'alert', msg: 'Error: ' + errors[i] };
            addAlert(alert);
        }
    };
};
