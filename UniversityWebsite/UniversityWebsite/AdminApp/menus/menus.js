﻿angular.module('configApp.menus', ['ui.router', 'configApp.menus.service', 'configApp.pages.service'])

    .config(
    [
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            var getMenus = function(menuService) {
                return menuService.allMenus();
            };

            $stateProvider
                .state('menus', {
                    url: '/menus',
                    templateUrl: 'adminapp/views/menus/menus.html',
                    resolve: {
                        menus: ['menuService', function(menuService) {
                            return menuService.allMenus();
                        }],
                    },
                    controller: 'menusEditCtrl',
                    data: { auth: "admin" }

                });
        }
    ]);