﻿angular.module('configApp.subjects')

.controller('subjectsEditCtrl', ['$scope', '$stateParams', '$modal', '$state', 'utils', 'subject', 'subjectsService', 'notificationFactory',
    function ($scope, $stateParams, $modal, $state, utils, subject, subjectsService, notificationFactory) {
        $scope.subject = subject;
        $scope.alerts = [];
        $scope.notificationService = notificationFactory($scope.alerts);
        initSections();

        $scope.editMode = false;
        if ($stateParams.subjectName == "newSubject") {
            $scope.editMode = true;
        }

        $scope.openSection = function (section) {
            if (!$scope.editMode) {
                section.open();
            }
        };

        //$scope.oneAtATime = true;

        $scope.tinymceOptions = {
            plugins: 'textcolor code advlist autolink lists link image charmap print preview anchor table',
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | forecolor backcolor | code",
            menu: {
                edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall' },
                insert: { title: 'Insert', items: 'link media | template hr | table' },
                format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat' },
            },
        };

        $scope.edit = function () {
            $scope.editMode = true;
        };


        /**
         * Opens confirmation window before delete current subject.
         */
        $scope.openDeleteModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'adminapp/views/partials/confirmation.modal.html',
                controller: 'deleteSubjectModalCtrl',
                resolve: {
                    subject: function () {
                        return $scope.subject;
                    },
                    subjects: function () {
                        return $scope.subjects;
                    }
                }
            });
            modalInstance.result.then(function () {
            }, function (error) {
                var alert = { type: 'alert', msg: 'Error: ' + "unable to remove subject" };
                $scope.notificationService.addAlert(alert);
            });
        };

        $scope.update = function () {
            utils.showValidation($scope.subjectForm);
            if ($scope.subjectForm.$valid) {
                if ($scope.subject.Id == null) {
                    $scope.subject.Syllabus = new Object();
                    $scope.subject.Schedule = new Object();
                    subjectsService.post($scope.subject, function (response) {
                        $scope.subject = response;
                        $scope.subjects.push($scope.subject);
                        $scope.editMode = false;
                        $state.go('subjects.edit', { subjectName: $scope.subject.Name, subject: $scope.subject });
                        addSuccesAlert();
                    }, $scope.notificationService.handleModelStateErrors);
                } else {
                    subjectsService.update({ id: $scope.subject.Id }, $scope.subject, function (response) {
                        $scope.subject = response;
                        $scope.editMode = false;
                        addSuccesAlert();
                    }, $scope.notificationService.handleModelStateErrors);
                }
            }
        };

        var addSuccesAlert = function () {
            var alert = { type: 'success', msg: 'The ' + $scope.subject.Name + ' subject has been updated.' };
            $scope.notificationService.addAlert(alert);
        };

        $scope.createNews = function () {
            var news = new Object();
            $scope.subject.News.push(news);
        };

        function initSections() {
            function section(name, url) {
                this.name = name;
                this.url = null;
                var hiddenUrl = url;
                this.open = function () {
                    this.url = hiddenUrl;
                };
                this.close = function () {
                    this.url = null;
                };
            }

            $scope.studentsSection = new section('students', 'adminapp/views/subjects/section.students.html');
            $scope.teachersSection = new section('teachers', 'adminapp/views/subjects/section.teachers.html');
            $scope.filesSection = new section('files', 'adminapp/views/subjects/section.files.html');
            $scope.requestsSection = new section('requests', 'adminapp/views/subjects/section.requests.html');
            $scope.newsSection = new section('news', 'adminapp/views/subjects/section.news.html');
            $scope.syllabusSection = new section('syllabus', 'adminapp/views/subjects/section.syllabus.html');
            $scope.scheduleSection = new section('schedule', 'adminapp/views/subjects/section.schedule.html');
        };

    }])