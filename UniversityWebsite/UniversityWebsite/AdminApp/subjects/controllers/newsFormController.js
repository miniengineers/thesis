﻿angular.module('configApp.subjects')

.controller('newsFormCtrl', ['$scope', '$sce', 'subjectsService', 'utils',
    function ($scope, $sce, subjectsService, utils) {
        $scope.newsMaxLength = 200;
        $scope.init = function (news) {
            $scope.formHide = true;
            if (news.Id == null) {
                $scope.formHide = false;
            }
        };

        $scope.updateNews = function (subject, news) {
            utils.showValidation($scope.form);
            if ($scope.form.$valid) {
                if (news.Id != null) {
                    subjectsService.updateNews({ subjectId: subject.Id, id: news.Id }, news, function (response) {
                        news.Id = response.Id;
                        news.Content = response.Content;
                        news.Header = response.Header;
                        $scope.formHide = true;
                        addSuccesAlert(news);
                    }, $scope.notificationService.handleModelStateErrors);
                    $scope.formHide = true;
                } else {
                    subjectsService.postNews({ subjectId: subject.Id }, news, function (response) {
                        news.Id = response.Id;
                        news.Content = response.Content;
                        news.Header = response.Header;
                        $scope.formHide = true;
                        addSuccesAlert(news);
                    }, $scope.notificationService.handleModelStateErrors);
                }
            }
        };

        var addSuccesAlert = function (news) {
            var alert = { type: 'success', msg: 'News ' + news.Header + ' has been updated.' };
            $scope.notificationService.addAlert(alert);
        };

        $scope.edit = function () {
            $scope.formHide = false;
        };

        $scope.hideForm = function () {
            $scope.formHide = true;
        };

        $scope.remove = function (subject, news) {
            subjectsService.removeNews(subject.Id, news.Id).then(function () {
                utils.removeByHeader(subject.News, news.Header);
                var alert = { type: 'success', msg: 'Selected news was successfully removed.' };
                $scope.notificationService.addAlert(alert);
            }, $scope.notificationService.handleModelStateErrors);

        };

        $scope.html = function (content) {
            return $sce.trustAsHtml(content);
        };
    }])