﻿angular.module('configApp.subjects')

.controller('deleteSubjectModalCtrl', ['$scope', '$modalInstance', '$state', 'subjectsService', 'subject', 'subjects', 'utils',
    function ($scope, $modalInstance, $state, subjectsService, subject, subjects, utils) {
        $scope.title = "Are you sure you want to remove this subject?";
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function () {
            subjectsService.removeSubject(subject.Id).then(function (resp) {
                $modalInstance.close("ok");
                utils.removeByName(subjects, subject.Name);
                $state.go('subjects');
            }, function (error) {
                $modalInstance.dismiss(error);
            });
        };

    }])