﻿angular.module('configApp.subjects')

.controller('subjectTeachersCtrl', ['$scope', 'subjectsService', '$modal', function ($scope, subjectsService, $modal) {
    loadTeachers();

    $scope.update = function (teacher) {
        var teacherIds = [];
        teacherIds.push(teacher.Id);
        subjectsService.updateTeachers($scope.subject.Id, teacherIds).then(function (resp) {
            var alert = { type: 'success', msg: 'Teacher ' + teacher.FirstName + ' ' + teacher.LastName + ' was successfully added to the subject.' };
            $scope.notificationService.addAlert(alert);
            loadTeachers();
        }, $scope.notificationService.handleModelStateErrors);
    };

    $scope.remove = function() {
        var selectedTeachers = Enumerable.From($scope.teachers).Where(function(teacher) {
            return teacher.selected;
        }).Select(function(teacher) {
            return teacher.Id;
        }).ToArray();

        if (selectedTeachers.length > 0) {
            subjectsService.removeTeachers($scope.subject.Id, selectedTeachers).then(function (resp) {
                loadTeachers();
                var alert = { type: 'success', msg: 'Selected teachers were successfully removed from the subject.' };
                $scope.notificationService.addAlert(alert);
            }, $scope.notificationService.handleModelStateErrors);
        }
    };

    $scope.add = function () {
        $scope.openUsersModal();
    };

    $scope.openUsersModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'adminapp/views/users/users.modal.html',
            controller: 'usersModalCtrl',
            windowClass: 'small'
        });

        modalInstance.result.then(function (selectedUser) {
            $scope.update(selectedUser);
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    function loadTeachers() {
        $scope.teachers = subjectsService.queryTeachers($scope.subject.Id);
    };
}]);