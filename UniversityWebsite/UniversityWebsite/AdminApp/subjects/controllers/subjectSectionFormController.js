﻿angular.module('configApp.subjects')

.controller('subjectSectionFormCtrl', ['$scope', '$sce', 'subjectsService', 'utils',
    function ($scope, $sce, subjectsService, utils) {

        $scope.formHide = true;

        $scope.updateSubject = function (subject) {
            console.log($scope.form);
            utils.showValidation($scope.form);
            if ($scope.form.$valid) {
                subjectsService.update({ id: subject.Id }, subject, function (response) {
                    $scope.editMode = false;
                    $scope.formHide = true;
                    addSuccesAlert();
                }, $scope.notificationService.handleModelStateErrors);
            }
        };

        var addSuccesAlert = function () {
            var alert = { type: 'success', msg: 'The ' + $scope.subject.Name + ' subject has been updated.' };
            $scope.notificationService.addAlert(alert);
        };

        $scope.edit = function () {
            $scope.formHide = false;
        };

        $scope.hideForm = function () {
            $scope.formHide = true;
        };
    }])