﻿angular.module('configApp.subjects')

.controller('subjectsCtrl', ['$scope', '$state', 'subjectsService', 'user',
    function ($scope, $state, subjectsService, user) {
        $scope.user = user;

        $scope.add = function () {
            $state.go("subjects.edit", { subjectName: 'newSubject', subject: { Id: null } });
        };

        $scope.subjectsChanged = function (subjects) {
            $scope.subjects = subjects;
        };
    }])