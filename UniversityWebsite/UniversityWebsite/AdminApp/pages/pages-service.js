﻿angular.module('configApp.pages.service', [

])

.factory('Pages', ['$resource', function ($resource) {
    var path = "/api/page";
    return $resource(path + '/:id', {}, {
        query: { method: 'GET'},
        post: { method: 'POST' },
        update: { method: 'PUT' },
        remove: { method: 'DELETE' }
    });
}])
.factory('pagesService', ['$http', 'utils',  'Pages', function ($http, utils, Pages) {
    var path = "/api/page";

    var factory = {};


    factory.update = Pages.update;
    factory.post = Pages.post;
    factory.remove = Pages.remove;

    factory.queryPages = function (limit, offset, lang) {
        return $http.get(path, { params: { lang: lang, limit: limit, offset: offset } });
    };

    factory.findPage = function (urlName) {
        return Pages.query({ id: urlName });
    };

    factory.searchPages = function(searchPattern, lang) {
        return $http.get(path, { params: { limit: 10, offset: 0, pattern: searchPattern, lang: lang} });
    };

    return factory;
}]);