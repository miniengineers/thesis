﻿angular.module('configApp.pages', ['ui.router', 'configApp.pages.service'])
    .config(
    [
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('pages', {
                    url: '/pages',
                    templateUrl: 'adminapp/views/pages/pages.html',
                    data: { auth: "admin"},
                    resolve: {
                        languages: ['languageService', function (languageService) {
                            return languageService.allLanguages().then(function (resp) {
                                return resp.data;
                            }, function (error) {
                                console.log(error);
                                return null;
                            });
                        }]
                    },
                    controller: [
                        '$scope', '$state', 'languages',
                        function($scope, $state, languages) {
                            $scope.languages = languages;

                            $scope.add = function() {
                                $state.go("pages.edit", { pageName: "newPage", page: { Id: null } });
                            };

                            $scope.pageChanged = function (pages) {
                                $scope.pages = pages;
                            };
                        }
                    ]
                })
                .state('pages.edit', {
                    url: '/:pageName',
                    data: { auth: "" },
                    params: {
                        page: null
                    },
                    views: {
                        '': {
                            templateUrl: 'adminapp/views/pages/pages.edit.html',
                            controller: 'pagesEditCtrl',
                            data: { auth: "admin"},
                            resolve: {
                                page: ["pagesService", "$stateParams",
                                    function (pagesService, $stateParams) {
                                        if ($stateParams.page != null && $stateParams.page.Id == null) {
                                            return $stateParams.page;
                                        }
                                        return pagesService.findPage($stateParams.pageName).$promise.then(function (response) {
                                            return response;
                                        }, function(error) {
                                            return null;
                                        });
                                }]
                            },
                        },
                    }
                });
        }
    ]);