﻿angular.module('galleryApp', [
    'mm.foundation',
    'angularUtils.directives.dirPagination',
    'ngAnimate',
    'angularSpinner',
    'configApp.gallery',
    'configApp.utils.service',
    'configApp.files.service',
    'ngResource'])

.filter("sanitize", ['$sce', function ($sce) {
    return function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    };
}])
.directive('ncgRequestVerificationToken', ['$http', function ($http) {
    return function (scope, element, attrs) {
        $http.defaults.headers.common['RequestVerificationToken'] = attrs.ncgRequestVerificationToken || "no request verification token";
    };
}])

