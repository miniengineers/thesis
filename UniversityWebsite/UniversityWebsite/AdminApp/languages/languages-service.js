﻿angular.module('configApp.languages.service', [
])

.factory('Languages', ['$resource', function ($resource) {
    return $resource('/api/languages/:lang', {}, {
        query: { method: 'GET', isArray: true },
        post: { method: 'POST' },
        update: { method: 'PUT' },
        remove: { method: 'DELETE' }
    });
}])

.factory('Dictionaries', ['$resource', function ($resource) {
    return $resource('/api/languages/dictionaries', {}, {
        query: { method: 'GET', isArray: true },
        post: { method: 'POST' },
        update: { method: 'PUT', isArray: true },
        remove: { method: 'DELETE' }
    });
}])

.factory('languageService', ['$http', 'utils', 'Dictionaries', 'Languages',
    function ($http, utils, Dictionaries, Languages) {
        var dictionariesPath = "/api/languages/dictionaries";
        var keysPath = "/api/languages/keys";
        var languagesPath = "/api/languages";

        var factory = {};

        factory.allDictionaries = function () {
            return $http.get(dictionariesPath);
        };

        factory.allTranslationKeys = function () {
            return $http.get(keysPath);
        };

        factory.allLanguages = function () {
            return $http.get(languagesPath);
        };

        factory.removeLanguage = function(language) {
            return Languages.remove({ lang: language.CountryCode }).$promise;
        };

        factory.updateDictionaries = Dictionaries.update;


        factory.queryLanguages = Languages.query;

        return factory;
    }]);