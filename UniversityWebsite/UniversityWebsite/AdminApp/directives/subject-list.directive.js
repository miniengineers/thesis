﻿configApp.directive('subjectList', function () {
    return {
        restrict: "E",
        templateUrl: "/adminapp/views/subjects/subject.list.html",
        scope: {
            selected: '&',
            changed: '&',
        },
        controller: ['$scope', '$state', 'subjectsService', 
            function ($scope, $state, subjectsService) {
            $scope.totalSubjects = 0;
            $scope.currentPage = 1;
            $scope.subjectsPerPage = 8;

            getPage(1);

            $scope.pageChanged = function (newPage) {
                getPage(newPage);
            };

            $scope.pagination = {
                current: 1
            };

            function getPage(pageNumber) {
                var offset = (pageNumber - 1) * $scope.subjectsPerPage;
                subjectsService.querySubjects($scope.subjectsPerPage, offset).then(function (response) {
                    $scope.totalSubjects = response.data.Number;
                    $scope.subjects = response.data.Elements;
                    if ($scope.changed() != null) {
                        $scope.changed()($scope.subjects);
                    }
                });
            };

            $scope.selectSubject = function (subject) {
                $state.go("subjects.edit", { subjectName: subject.UrlName, subject: subject });
            };
        }]
    };
});