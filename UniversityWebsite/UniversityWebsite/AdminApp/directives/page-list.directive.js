﻿configApp.directive('pageList', function () {
    return {
        restrict: "E",
        templateUrl: "/adminapp/views/pages/pages.list.html",
        scope: {
            selected: '&',
            changed: '&',
            showTitle: '=',
            lang: '=',
        },
        link: function (scope, element, attrs) {
            scope.$watch('lang', function (newValue, oldValue) {
                if (newValue) {
                    scope.getPage(1);
                }
            }, true);
        },
        controller: ['$scope', 'pagesService', function ($scope, pagesService) {
            $scope.totalPages = 0;
            $scope.currentPage = 1;
            $scope.pagesPerPage = 8;
            $scope.actionAvailable = $scope.selected() != null;

            $scope.getPage = function (pageNumber) {
                return getPage(pageNumber);
            };

            getPage(1);

            $scope.pageChanged = function (newPage) {
                getPage(newPage);
            };

            $scope.pagination = {
                current: 1
            };

            function getPage(pageNumber) {
                var offset = (pageNumber - 1) * $scope.pagesPerPage;
                pagesService.queryPages($scope.pagesPerPage, offset, $scope.lang).then(function (response) {
                    $scope.totalPages = response.data.Number;
                    $scope.pages = response.data.Elements;
                    if ($scope.changed() != null) {
                        $scope.changed()($scope.pages);
                    }
                });
            };

            $scope.select = function (page) {
                $scope.selected()(page);
            };
        }]
    };
});