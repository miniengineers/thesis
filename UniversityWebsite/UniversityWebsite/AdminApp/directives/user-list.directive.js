﻿configApp.directive('userList', function () {
    return {
        restrict: "E",
        templateUrl: "/adminapp/views/users/user.list.html",
        scope: {
            showTitle: '=',
            role: '=',
            filter: '=',
            doubleClick: '&'
        },
        controller: ['$scope', 'userService', function ($scope, userService) {
            $scope.totalUsers = 0;
            $scope.currentPage = 1;
            $scope.usersPerPage = 8;
            $scope.userStatuses = ['all', 'confirmed', 'not-confirmed'];
            $scope.userStatus = $scope.userStatuses[0];
            var userConfirmed = null;

            getPage(1);

            $scope.pageChanged = function (newPage) {
                getPage(newPage);
            };

            $scope.pagination = {
                current: 1
            };

            function getPage(pageNumber) {
                var offset = (pageNumber - 1) * $scope.usersPerPage;
                userService.queryUsers($scope.role, $scope.usersPerPage, offset, userConfirmed).then(function (response) {
                    $scope.totalUsers = response.data.Number;
                    $scope.users = response.data.Elements;
                });
            };

            $scope.select = function (user) {
                $scope.doubleClick()(user);
            };

            $scope.updateList = function() {
                switch($scope.userStatus) {
                    case 'all':
                        userConfirmed = null;
                        break;
                    case 'confirmed':
                        userConfirmed = true;
                        break;
                    case 'not-confirmed':
                        userConfirmed = false;
                        break;
                    default:
                        userConfirmed = null;
                }
            };
        }]
    };
});