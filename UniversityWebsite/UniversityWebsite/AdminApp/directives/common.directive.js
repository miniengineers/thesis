﻿configApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

configApp.directive('ncgRequestVerificationToken', [
    '$http', function($http) {
        return function(scope, element, attrs) {
            $http.defaults.headers.common['RequestVerificationToken'] = attrs.ncgRequestVerificationToken || "no request verification token";
        };
    }
]);

configApp.directive('loading', ['$http', 'usSpinnerService', function ($http, usSpinnerService) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    usSpinnerService.spin('spinner-1');
                } else {
                    usSpinnerService.stop('spinner-1');
                }
            });
        }
    };
}]);

configApp.directive('notification', ['$timeout' , function ($timeout) {
    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            service: '=',
            index: '='
        },
        template: '<alert type="ngModel.type" close="service.closeAlert(index)">{{ngModel.msg}}</alert>',
        link: function (scope, element, attrs) {
            if (!scope.ngModel.timeoutDisabled) {
                $timeout(function () {
                    element.remove();
                }, 5000);
            }
        }
    };
}]);