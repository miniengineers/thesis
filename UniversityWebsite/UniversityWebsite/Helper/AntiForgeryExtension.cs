﻿using System;
using System.Web.Mvc;

namespace UniversityWebsite.Helper
{
    /// <summary>
    /// Pomaga przy walidacji AntiForgeryTokenu pochodzżcego z części systemu wykorzystującej AngularJS.
    /// </summary>
    public static class AntiForgeryExtension
    {
        /// <summary>
        /// Przygotowuje antiforgerytoken do walidacji.
        /// </summary>
        /// <param name="helper">Obiekt klasy HtmlHelper</param>
        /// <returns>Wartość tokenu.</returns>
        public static string RequestVerificationToken(this HtmlHelper helper)
        {
            return String.Format("ncg-request-verification-token={0}", GetTokenHeaderValue());
        }

        private static string GetTokenHeaderValue()
        {
            string cookieToken, formToken;
            System.Web.Helpers.AntiForgery.GetTokens(null, out cookieToken, out formToken);
            return cookieToken + ":" + formToken;
        }
    }
}